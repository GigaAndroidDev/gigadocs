//
//  DoctorHomeController.h
//  GigaDocs
//
//  Created by RIR Technologies on 12/21/17.
//  Copyright © 2017 RIR Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"
@interface DoctorHomeController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,DropDownViewDelegate>
{
    DropDownView *dropDownView;

}

@property  (weak,nonatomic) IBOutlet UITextField *hospitalName;
@property  (weak,nonatomic) IBOutlet UIView *calenderParentView;
@property  (weak,nonatomic) IBOutlet UITableView *tableView;
@property  (weak,nonatomic) IBOutlet UITableView *menuTableView;
@property  (weak,nonatomic) IBOutlet UIView *menuView;
@property  (weak,nonatomic) IBOutlet UILabel *patientName;
@property  (weak,nonatomic) IBOutlet UILabel *phoneNum;
@property  (weak,nonatomic) IBOutlet UILabel *patientAddress;
@property  (weak,nonatomic) IBOutlet UILabel *emailId;
@property  (weak,nonatomic) IBOutlet UILabel *procedure;
@property  (weak,nonatomic) IBOutlet UIView *patientView;
@property  (weak,nonatomic) IBOutlet UIImageView *background;
@property  (weak,nonatomic) IBOutlet UIButton *hospitalBtn;
@property (weak,nonatomic) IBOutlet UIView *activityView;

@end
