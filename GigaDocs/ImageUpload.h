//
//  ImageUpload.h
//  GigaDocs
//
//  Created by RIR Technologies on 1/6/18.
//  Copyright © 2018 RIR Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageUpload : NSString

-(void)imageupload:(NSString*)encoed
            accept:(NSString*)acceptType;
@property NSDictionary *dictionary;

@end
