//
//  LoginViewController.m
//  GigaDocs
//
//  Created by RIR Technologies on 12/21/17.
//  Copyright © 2017 RIR Technologies. All rights reserved.
//

#import "LoginViewController.h"
#import "DoctorHomeController.h"
#import "RegisterViewController.h"
#import "ForgotViewController.h"
#import "GMDCircleLoader.h"
#import "ClinicAvailabeController.h"
@interface LoginViewController ()
{
    NSString *userType;
    NSString *remember;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.background setImage:[self gradientforview:self.background.bounds] ];

    self.activityView.hidden=YES;
    [GMDCircleLoader setOnView:self.activityView withTitle:@"" animated:YES];

    
    self.emailTf.delegate=self;
    self.passwordTf.delegate=self;

        userType=@"DOCTOR";

    self.login.clipsToBounds=YES;
    self.login.layer.cornerRadius=1.0f;
    self.login.layer.borderWidth=1.0f;
    self.login.layer.borderColor=[UIColor colorWithRed:52.0/255.0 green:181.0/255.0 blue:236.0/255.0 alpha:1.0f].CGColor;
    
    
    
    self.signup.clipsToBounds=YES;
    self.signup.layer.cornerRadius=1.0f;
    self.signup.layer.borderWidth=1.0f;
    self.signup.layer.borderColor=[UIColor colorWithRed:52.0/255.0 green:181.0/255.0 blue:236.0/255.0 alpha:1.0f].CGColor;
    
   
    
      self.passwordTf.rightViewMode = UITextFieldViewModeAlways;
      self.passwordTf.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password.png"]];
    
    
   
    self.emailTf.rightViewMode = UITextFieldViewModeAlways;
    self.emailTf.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mail.png"]];
    
    
    NSString *remem=[[NSUserDefaults standardUserDefaults]objectForKey:@"remember"];
    if ([remem isEqualToString:@"yes"])
    {
        
      self.emailTf.text=  [[NSUserDefaults standardUserDefaults]objectForKey:@"email_rem"];
        
        self.passwordTf.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"password_rem"];
        
        userType=[[NSUserDefaults standardUserDefaults]objectForKey:@"usertype"];
        if ([userType isEqualToString:@"DOCTOR"])
        {
            
            [self.doctor setImage:[UIImage imageNamed:@"scheck.png"] forState:UIControlStateNormal];
            
        }
        else
        {
            [self.hospital setImage:[UIImage imageNamed:@"scheck.png"] forState:UIControlStateNormal];
 
        }

        remember=@"yes";

        
        
        [self.remeber setImage:[UIImage imageNamed:@"scheck.png"] forState:UIControlStateNormal];
        
    }
    else{
        [self.doctor setImage:[UIImage imageNamed:@"scheck.png"] forState:UIControlStateNormal];

    }
}

-(IBAction)loginClicked:(id)sender
{
    
    if (self.emailTf.text.length==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please Enter Email Address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if (self.passwordTf.text.length==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please Enter a password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if (self.passwordTf.text.length !=6)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please Enter a valid Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else
        
    {self.activityView.hidden=NO;
        
        NSString *security=[[NSUserDefaults standardUserDefaults]objectForKey:@"securityKey"];
        
    NSString *myRequestString = [[NSString alloc] initWithFormat:@"security_key=%@&email_id=%@&password=%@&account_type=%@",security,self.emailTf.text,self.passwordTf.text,userType];
        
        NSLog(@"my req==%@",myRequestString);
        
        NSData *myRequestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
        
        NSMutableURLRequest *request =[[NSMutableURLRequest alloc]initWithURL:[ NSURL URLWithString:@"http://www.gigadocs.com/giga_api/login"]];
        
        [request setHTTPMethod: @"POST"];
        
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        
        [request setHTTPBody: myRequestData];
        
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        
        
        
        NSURLSessionDataTask * dataTask1 =[defaultSession dataTaskWithRequest:request completionHandler:^(NSData* data, NSURLResponse* response, NSError *error) {                                                          if(error == nil)
            
        {
            
            self.activityView.hidden=YES;
            
            NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            self.activityView.hidden=YES;
            
            
            
            NSLog(@"reslt==%@",result);
            
            NSString *status=[result valueForKey:@"Status"];
            
            if ([status isEqualToString:@"true"])
                
            {
                
                
                if ([remember isEqualToString:@"yes"])
                {
                    [[NSUserDefaults standardUserDefaults]setObject:self.emailTf.text forKey:@"email_rem"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:self.passwordTf.text forKey:@"password_rem"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:userType forKey:@"usertype"];

                    [[NSUserDefaults standardUserDefaults]setObject:remember forKey:@"remember"];
                    
                    
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"email_rem"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"password_rem"];
                    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"usertype"];

                    
                    [[NSUserDefaults standardUserDefaults]setObject:remember forKey:@"remember"];
                    
                    
                }
                NSDictionary *dict=[[result valueForKey:@"message"]objectAtIndex:0];
                NSLog(@"dict==%@",dict);
                
                
                
                if ([userType isEqualToString:@"hospital"])
                {
                   
                    [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"listing_id"] forKey:@"userid"];
  
//                    [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"registration_no"] forKey:@"userid"];
   
    ClinicAvailabeController *clini=[[ClinicAvailabeController alloc]initWithNibName:@"ClinicAvailabeController" bundle:nil];
[self.navigationController pushViewController:clini animated:YES];
                    
                }
                else
                {
                
                
                [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"listing_id"] forKey:@"userid"];
                
                
                [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"name"] forKey:@"name"];

                
                [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"description"] forKey:@"descrip"];
                    
                    
                    
                    DoctorHomeController *reg=[[DoctorHomeController alloc] initWithNibName:@"DoctorHomeController" bundle:nil];
                    [self.navigationController pushViewController:reg animated:YES];
                    
   
                }
                
                
                
//                [[NSUserDefaults standardUserDefaults]synchronize];
                
                
                        }
            
            else
                
            {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[result valueForKey:@"message"] message:nil delegate:self cancelButtonTitle:@"OK"otherButtonTitles: nil];
                
                [alert show];
                
            }
            
        }
            
            
            
        }];
        
        [dataTask1 resume];
    }

   
}
-(IBAction)signupClicked:(id)sender
{
    RegisterViewController *reg=[[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    [self.navigationController pushViewController:reg animated:YES];
}
-(IBAction)remberClicked:(id)sender
{
    
    if ([self.remeber.imageView.image isEqual:[UIImage imageNamed:@"check.png"]])
    {
        [self.remeber setImage:[UIImage imageNamed:@"scheck.png"] forState:UIControlStateNormal];
        remember=@"yes";
        

    }
    else
    {
        [self.remeber setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        remember=@"no";


    }
    
}
-(IBAction)doctorClicked:(id)sender
{
//    if ([self.doctor.imageView.image isEqual:[UIImage imageNamed:@"check.png"]])
//    {
//        [self.doctor setImage:[UIImage imageNamed:@"scheck.png"] forState:UIControlStateNormal];
    
        userType=@"DOCTOR";
        
//    }
//    else
//    {
//        [self.doctor setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
//        userType=@"PATIENT";
//
//    }
    
    [self.doctor setImage:[UIImage imageNamed:@"scheck.png"] forState:UIControlStateNormal];
    
    [self.hospital setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
    
    [self.patient setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];

}
-(IBAction)clincClicked:(id)sender
{
    userType=@"hospital";
    
    [self.doctor setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];

    [self.hospital setImage:[UIImage imageNamed:@"scheck.png"] forState:UIControlStateNormal];

    [self.patient setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];


}
-(IBAction)patientClicked:(id)sender
{
    userType=@"PATIENT";
    [self.doctor setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
    
    [self.hospital setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
    
    [self.patient setImage:[UIImage imageNamed:@"scheck.png"] forState:UIControlStateNormal];

    
}
-(IBAction)forgotClicked:(id)sender
{
    ForgotViewController *reg=[[ForgotViewController alloc] initWithNibName:@"ForgotViewController" bundle:nil];
    [self.navigationController pushViewController:reg animated:YES];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [textField resignFirstResponder];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.emailTf resignFirstResponder];
    [self.passwordTf resignFirstResponder];
}
-(UIImage*)gradientforview:(CGRect)bounds
{
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = bounds;
       gradient.colors = @[(id)[UIColor colorWithRed:84.0/255.0 green:218.0/255.0 blue:221.0/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:65.0/255.0 green:201.0/255.0 blue:253.0/255.0 alpha:1.0].CGColor];
    
    gradient.startPoint=CGPointMake(0.0, 0.0);
    gradient.endPoint = CGPointMake(0.5, 1.0);
    UIGraphicsBeginImageContext(bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *aimage=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return aimage;
}
@end
