//
//  ClinicAvailabeController.h
//  GigaDocs
//
//  Created by RIR Technologies on 1/2/18.
//  Copyright © 2018 RIR Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"
@interface ClinicAvailabeController : UIViewController<DropDownViewDelegate>
{
    DropDownView *dropDownView;

}

@property (weak,nonatomic) IBOutlet UIImageView *bg;
@property (weak,nonatomic) IBOutlet UITextField *doctor;
@property (weak,nonatomic) IBOutlet UIButton *slot;
@property (weak,nonatomic) IBOutlet UIButton *dropBtn;

@property (weak,nonatomic) IBOutlet UIView *activityView;

@end
