//
//  ForgotViewController.h
//  GigaDocs
//
//  Created by RIR Technologies on 12/21/17.
//  Copyright © 2017 RIR Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotViewController : UIViewController<UITextFieldDelegate>

@property (weak,nonatomic) IBOutlet UITextField *emailTf;
@property (weak,nonatomic) IBOutlet UIButton *reset;
@property (weak,nonatomic) IBOutlet UIImageView *background;
@property (weak,nonatomic) IBOutlet UIView *activityView;

@end
