//
//  ImageFileUpload.h
//  GigaDocs
//
//  Created by RIR Technologies on 1/11/18.
//  Copyright © 2018 RIR Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageFileUpload : UIView
@property NSDictionary *dictionary;
-(NSString*)imageupload:(UIImage*)image
            accept:(NSString*)acceptType;

@property NSString *string;
@end
