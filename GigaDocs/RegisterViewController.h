//
//  RegisterViewController.h
//  GigaDocs
//
//  Created by RIR Technologies on 12/21/17.
//  Copyright © 2017 RIR Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController<UITextFieldDelegate>


@property (weak,nonatomic) IBOutlet UITextField *emailTf;
@property (weak,nonatomic) IBOutlet UITextField *mobileTf;
@property (weak,nonatomic) IBOutlet UITextField *passwordTf;
@property (weak,nonatomic) IBOutlet UITextField *nameTf;
@property (weak,nonatomic) IBOutlet UITextField *licenseTf;
@property (weak,nonatomic) IBOutlet UIButton *signup;
@property (weak,nonatomic) IBOutlet UIButton *login;
@property (weak,nonatomic) IBOutlet UIButton *doctor;
@property (weak,nonatomic) IBOutlet UIImageView *background;
@property (weak,nonatomic) IBOutlet UIView *activityView;

@end
