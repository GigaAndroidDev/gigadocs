//
//  ForgotViewController.m
//  GigaDocs
//
//  Created by RIR Technologies on 12/21/17.
//  Copyright © 2017 RIR Technologies. All rights reserved.
//

#import "ForgotViewController.h"
#import "LoginViewController.h"
@interface ForgotViewController ()

@end

@implementation ForgotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.emailTf.delegate=self;

    
    
    self.reset.clipsToBounds=YES;
    self.reset.layer.cornerRadius=5.0f;
    self.reset.layer.borderWidth=1.0f;
    self.reset.layer.borderColor=[UIColor colorWithRed:155.0/255.0 green:10.0/255.0 blue:0.0/255.0 alpha:1.0f].CGColor;

    
    self.emailTf.rightViewMode = UITextFieldViewModeAlways;
    self.emailTf.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mail.png"]];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [textField resignFirstResponder];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.emailTf resignFirstResponder];
}
-(IBAction)resetClicked:(id)sender
{
    LoginViewController *reg=[[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    [self.navigationController pushViewController:reg animated:YES];

}
-(IBAction)backClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(UIImage*)gradientforview:(CGRect)bounds
{
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:0.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:52.0/255.0 green:181.0/255.0 blue:236.0/255.0 alpha:1.0].CGColor];
    
    
    gradient.startPoint=CGPointMake(0.0, 0.0);
    gradient.endPoint = CGPointMake(0.5, 1.0);
    UIGraphicsBeginImageContext(bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *aimage=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return aimage;
}
@end
