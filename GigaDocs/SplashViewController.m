//
//  SplashViewController.m
//  GigaDocs
//
//  Created by RIR Technologies on 12/21/17.
//  Copyright © 2017 RIR Technologies. All rights reserved.
//

#import "SplashViewController.h"
#import "LoginViewController.h"
@interface SplashViewController ()

@end

@implementation SplashViewController

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
}
-(void)viewDidAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.background setImage:[self gradientforview:self.background.bounds] ];

    
    [UIView animateWithDuration:1.8
                          delay:0.1
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         
//                         self.image.frame = CGRectMake(self.image.frame.origin.x, 150, self.image.frame.size.width , self.image.frame.size.height);
                     }
                     completion:^(BOOL finished){
                     }];
    
    
    [self performSelector:@selector(startAnimating) withObject:self afterDelay:1.9];
}

-(UIImage*)gradientforview:(CGRect)bounds
{
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:84.0/255.0 green:218.0/255.0 blue:221.0/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:65.0/255.0 green:201.0/255.0 blue:253.0/255.0 alpha:1.0].CGColor];
    
    
    gradient.startPoint=CGPointMake(0.0, 0.0);
    gradient.endPoint = CGPointMake(0.5, 1.0);
    UIGraphicsBeginImageContext(bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *aimage=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return aimage;
}

- (void)startAnimating
{
    
    LoginViewController *reg=[[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    [self.navigationController pushViewController:reg animated:YES];
}

@end
