//
//  Error.swift
//  NeuralNet-MNIST
//
//  Created by Collin Hundley on 4/22/17.
//
//

import Foundation


public extension NeuralNet {
    
    public enum ErrorFunction {
      
        case meanSquared
        case crossEntropy
       
        case percentage
              case custom(error: ((_ real: [Float], _ target: [Float], _ rows: Int, _ cols: Int) -> Float))
        
        
              public func computeError(real: [Float], target: [Float], rows: Int, cols: Int) -> Float {
            switch self {
            case .meanSquared:
                // 1 / n * ∑( (a[i] - t[i]) * (a[i] - t[i]) )
                let sum = zip(real, target).reduce(0) { (sum, pair) -> Float in
                    return (pair.0 - pair.1) * (pair.0 - pair.1)
                }
                return sum / Float(real.count)
            case .crossEntropy:
                // -1 / n * ∑( [t[i] * ln(a[i]) + (1 − t[i]) * ln(1 − a[i])] )
                return -zip(real, target).reduce(0) { (sum, pair) -> Float in
                    let temp = pair.1 * log(pair.0)
                    return sum + temp + (1 - pair.1) * log(1 - pair.0)
                    } / Float(real.count)
            case .percentage:
                // incorrect / batchSize
                var incorrect = 0
                // Iterate through each set of outputs/labels
                for row in 0..<rows {
                    let start = row * cols
                    let realSet = real[start..<(start + cols)]
                    let labelSet = target[start..<(start + cols)]
                    // Compare max values of outputs and labels
                    if realSet.index(of: realSet.max()!) != labelSet.index(of: labelSet.max()!) {
                        // Incorrect answer; increment counter
                        incorrect += 1
                    }
                }
                return Float(incorrect) / Float(rows)
            case .custom(let error):
                return error(real, target, rows, cols)
            }
        }
        
    }
    
}
