//
//  ImageFileUpload.m
//  GigaDocs
//
//  Created by RIR Technologies on 1/11/18.
//  Copyright © 2018 RIR Technologies. All rights reserved.
//

#import "ImageFileUpload.h"

@implementation ImageFileUpload

- (NSString*)imageupload:(UIImage*)image
              accept:(NSString*)acceptType
{
   
        
        
        NSString *security=[[NSUserDefaults standardUserDefaults]objectForKey:@"securityKey"];
        NSString *userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        
        NSString *hospitalid=[[NSUserDefaults standardUserDefaults]objectForKey:@"hospoita_id"];
        
        
        
        
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        
        [_params setObject: security forKey:@"security_key"];
        [_params setObject:acceptType forKey:@"patient_accept"];
        [_params setObject:[self.dictionary valueForKey:@"apo_id"] forKey:@"appoint_id"];
        [_params setObject:@"" forKey:@"medicine"];
        [_params setObject:hospitalid forKey:@"user_id"];
        [_params setObject:[self.dictionary valueForKey:@"patient_id"] forKey:@"patient_id"];
        [_params setObject:@"1" forKey:@"camera_status"];
        [_params setObject:[self.dictionary valueForKey:@"doctor_id"] forKey:@"doctor_id"];
        
        
        NSLog(@"param====%@",_params);
        
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        NSString* FileParamConstant = @"attach";
        NSURL* requestURL = [NSURL URLWithString:@"http://www.gigadocs.com/giga_api/addprescription"];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        // post body
        NSMutableData *body = [NSMutableData data];
        // add params (all params are strings)
        for (NSString *param in _params)
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        
        
        
        NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
        if (imageData)
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imageData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setURL:requestURL];
        NSLog(@"requestt===%@",requestURL);
        
        NSURLResponse *response;
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
        if(returnData ==nil)
        {
            UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Something Went Wrong Please Try Again " delegate:self cancelButtonTitle:nil otherButtonTitles:@"Try again", nil];
            Alert.tag=1;
            [Alert show];
        }
        else
        {
            
            NSError *error;
            NSDictionary *result1 = [NSJSONSerialization JSONObjectWithData:returnData
                                                                    options:kNilOptions
                                     
                                                                      error:&error];
            NSLog(@"result=%@",result1);
            
            if ([[result1 valueForKey:@"Status"]integerValue]==0)
            {

                self.string=@"true";
                
            }
            else{
                self.string=@"false";

            }
        }
    

    
    return self.string;
 
}


@end
