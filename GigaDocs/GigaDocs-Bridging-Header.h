//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//
@class Activation;
@class Cache;
@class Dataset;
@class Error;
@class Storage;
@class Structure;
@class NeuralNet;
@class Colors;
@class ShadowView;
@class MainView;
@class   SampleViewController;
@class DoctorHomeController;
#import "DoctorHomeController.h"
#import "ImageUpload.h"
#import "ImageFileUpload.h"
