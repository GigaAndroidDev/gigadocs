//
//  CalendarCell.h
//  GigaDocs
//
//  Created by RIR Technologies on 12/21/17.
//  Copyright © 2017 RIR Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarCell : UITableViewCell

@property (weak,nonatomic) IBOutlet UILabel *nameL;
@property (weak,nonatomic) IBOutlet UILabel *timeL;
@property (weak,nonatomic) IBOutlet UILabel *typeL;
@property (weak,nonatomic) IBOutlet UIButton *camera;
@property (weak,nonatomic) IBOutlet UIButton *viewB;

@end
