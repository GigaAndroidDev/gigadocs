//
//  DoctorFileController.m
//  GigaDocs
//
//  Created by RIR Technologies on 12/21/17.
//  Copyright © 2017 RIR Technologies. All rights reserved.
//

#import "DoctorFileController.h"
#import "DoctorHomeController.h"
#import "GMDCircleLoader.h"
@interface DoctorFileController ()
{
    NSMutableArray *imageToUpload;
    NSMutableArray *imageArr;
    NSString *acceptType;


}

@end

@implementation DoctorFileController

- (void)viewDidLoad {
    [super viewDidLoad];
    imageToUpload=[[NSMutableArray alloc]init];
    imageArr=[[NSMutableArray alloc]init];
    [imageArr addObject:@"box.png"];

    [self.background setImage:[self gradientforview:self.background.bounds] ];
    acceptType=@"0";
    
    NSLog(@"dict===%@",self.dictionary);

    self.activityView.hidden=YES;
    [GMDCircleLoader setOnView:self.activityView withTitle:@"" animated:YES];

    self.patientName.text=[NSString stringWithFormat:@"%@",[self.dictionary valueForKey:@"patient_name"]];
    self.phoneNum.text=[NSString stringWithFormat:@"%@",[self.dictionary valueForKey:@"phone_mobile"]];
    self.emailId.text=[NSString stringWithFormat:@"%@",[self.dictionary valueForKey:@"email_id"]];
    self.patientAddress.text=[NSString stringWithFormat:@"%@",[self.dictionary valueForKey:@"patient_address"]];
    
    



}

-(IBAction)cameraClicked:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"CHOOSE" message:@"The Images Source!" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"PHOTOLIBRARY",@"CAMERA", nil];
    alert.tag=2;
    [alert show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 2)
    {
        
        cameraPic=[[UIImagePickerController alloc]init];
        cameraPic.delegate=self;
        [cameraPic setSourceType:UIImagePickerControllerSourceTypeCamera];
        [self presentViewController:cameraPic animated:YES completion:nil];
        
        // Set buttonIndex == 0 to handel "Ok"/"Yes" button response
        // Cancel button response
    }
    else if (buttonIndex == 1)
    {
        photoGalleryImage=[[UIImagePickerController alloc]init];
        photoGalleryImage.delegate=self;
        [photoGalleryImage setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [self presentViewController:photoGalleryImage animated:YES completion:nil];
        
    }
    else
    {
        [alertView removeFromSuperview];
    }

}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info


{
    [self dismissViewControllerAnimated:NO completion:nil];
    
    if (picker.sourceType==UIImagePickerControllerSourceTypeCamera)
    {
        
        
        UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        
        // Save image
        UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
        _cameraImage.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        
    }
    else
    {
        _cameraImage.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    }
//    UIImage *image=[self resizeImage:_coverPicIV.image];
//    NSLog(@"image=====%@",image);
    
    
    NSLog(@"image array=====%@",imageToUpload);
    
    NSData * imageData1 = UIImagePNGRepresentation(_cameraImage.image);
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    
    UIImage *image=[self resizeImage:_cameraImage.image];
    
    
    _cameraImage.image=image;

    
    [imageToUpload addObject:_cameraImage.image];

    
    NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",image]];
    
    [imageArr insertObject:fullPath atIndex:1] ;
    
    [fileManager createFileAtPath:fullPath contents:imageData1 attributes:nil];
    NSLog(@"IMAGENAME:%@",fullPath);
    
    [self viewWillAppear:YES];
    
    
}
-(UIImage *)resizeImage:(UIImage *)image
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = [UIScreen mainScreen].bounds.size.height;
    float maxWidth = [UIScreen mainScreen].bounds.size.width;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
    
}


-(IBAction)saveClicked:(id)sender
{
    [self.save setImage:[UIImage imageNamed:@"scheck.png"] forState:UIControlStateNormal];
    
    [self screenshot];
}
-(IBAction)acceptClicked:(id)sender
{
    
    if ([self.accept.imageView.image isEqual:[UIImage imageNamed:@"check.png"]])
    {
        [self.accept setImage:[UIImage imageNamed:@"scheck.png"] forState:UIControlStateNormal];
        acceptType=@"1";

    }
    else
    {
        [self.accept setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        acceptType=@"0";


    }
}
-(IBAction)clearClicked:(id)sender
{
    [self viewDidLoad];
    
}
- (void) screenshot {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    UIGraphicsBeginImageContext(screenRect.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [[UIColor blackColor] set];
    CGContextFillRect(ctx, screenRect);
    
    // grab reference to our window
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    
    // transfer content into our context
    [window.layer renderInContext:ctx];
    UIImage *screengrab = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    CGRect rect = CGRectMake(0, 65,self.view.frame.size.width,self.view.frame.size.height-120);
    
   

    CGImageRef imageRef = CGImageCreateWithImageInRect([screengrab CGImage], rect);
    UIImage *img = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    self.cameraImage.image =img;
    
   
    [self imageUpload];

    
   

}

-(UIImage*)gradientforview:(CGRect)bounds
{
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:0.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:52.0/255.0 green:181.0/255.0 blue:236.0/255.0 alpha:1.0].CGColor];
    
    
    gradient.startPoint=CGPointMake(0.0, 0.0);
    gradient.endPoint = CGPointMake(0.5, 1.0);
    UIGraphicsBeginImageContext(bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *aimage=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return aimage;
}
-(IBAction)backClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)imageUpload
{
    if (imageArr.count==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"error" message:@"Please Select image" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        
    
        self.activityView.hidden=NO;

        NSString *security=[[NSUserDefaults standardUserDefaults]objectForKey:@"securityKey"];
        NSString *userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        
        NSString *hospitalid=[[NSUserDefaults standardUserDefaults]objectForKey:@"hospoita_id"];
        
        

    
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        
        [_params setObject: security forKey:@"security_key"];
        [_params setObject:acceptType forKey:@"patient_accept"];
        [_params setObject:[self.dictionary valueForKey:@"apo_id"] forKey:@"appoint_id"];
        [_params setObject:@"" forKey:@"medicine"];
        [_params setObject:hospitalid forKey:@"user_id"];
        [_params setObject:[self.dictionary valueForKey:@"patient_id"] forKey:@"patient_id"];
        [_params setObject:@"1" forKey:@"camera_status"];
        [_params setObject:[self.dictionary valueForKey:@"doctor_id"] forKey:@"doctor_id"];

        
        NSLog(@"param====%@",_params);

            NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
            
            NSString* FileParamConstant = @"attach";
            NSURL* requestURL = [NSURL URLWithString:@"http://www.gigadocs.com/giga_api/addprescription"];
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
            [request setHTTPShouldHandleCookies:NO];
            [request setTimeoutInterval:30];
            [request setHTTPMethod:@"POST"];
        
        
            
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
            [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            // post body
            NSMutableData *body = [NSMutableData data];
            // add params (all params are strings)
            for (NSString *param in _params) {
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            
            
            
            NSData *imageData = UIImageJPEGRepresentation(_cameraImage.image, 1.0);
            if (imageData)
            {
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:imageData];
                [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [request setHTTPBody:body];
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setURL:requestURL];
            NSLog(@"requestt===%@",requestURL);
            
            NSURLResponse *response;
            NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
            if(returnData ==nil)
            {
                UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Something Went Wrong Please Try Again " delegate:self cancelButtonTitle:nil otherButtonTitles:@"Try again", nil];
                Alert.tag=1;
                [Alert show];
            }
            else
            {
                
                NSError *error;
               NSDictionary *result1 = [NSJSONSerialization JSONObjectWithData:returnData
                                                          options:kNilOptions
                           
                                                            error:&error];
                NSLog(@"result=%@",result1);
                
            if ([[result1 valueForKey:@"Status"]integerValue]==0)
                {
                    DoctorHomeController *home=[[DoctorHomeController alloc]initWithNibName:@"DoctorHomeController" bundle:nil];
                    [self.navigationController pushViewController:home animated:YES];
                    
                }
            }
        }
         
    
}
-(void)upload
{
    NSString *security=[[NSUserDefaults standardUserDefaults]objectForKey:@"securityKey"];
    
    NSData *imageData = UIImageJPEGRepresentation(_cameraImage.image, 1.0);
    
    NSString *str=[imageData base64Encoding];
    

    NSString *hospitalid=[[NSUserDefaults standardUserDefaults]objectForKey:@"hospoita_id"];

    
    NSString *myRequestString = [[NSString alloc] initWithFormat:@"security_key=%@&patient_accept=%@&appoint_id=%@&medicine=%@&user_id=%@&patient_id=%@&camera_status=%@&doctor_id=%@&attach=%@",security,acceptType,[self.dictionary valueForKey:@"apo_id"],@"",hospitalid,[self.dictionary valueForKey:@"patient_id"],@"0",[self.dictionary valueForKey:@"doctor_id"],str];
    
    NSLog(@"my req==%@",myRequestString);
    
    NSData *myRequestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc]initWithURL:[ NSURL URLWithString:@"http://www.gigadocs.com/giga_api/addprescription"]];
    
    [request setHTTPMethod: @"POST"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    [request setHTTPBody: myRequestData];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    
    
    NSURLSessionDataTask * dataTask1 =[defaultSession dataTaskWithRequest:request completionHandler:^(NSData* data, NSURLResponse* response, NSError *error) {                                                          if(error == nil)
        
    {
        
        self.activityView.hidden=YES;
        
        NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        self.activityView.hidden=YES;
        
        
        
        NSLog(@"reslt==%@",result);
        
        
        
        
    }
        
        
        
    }];
    
    [dataTask1 resume];


}

@end
