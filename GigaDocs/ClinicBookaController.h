//
//  ClinicBookaController.h
//  GigaDocs
//
//  Created by RIR Technologies on 1/1/18.
//  Copyright © 2018 RIR Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"
@interface ClinicBookaController : UIViewController<UITextFieldDelegate,DropDownViewDelegate>
{
    DropDownView *dropDownView;

}
@property (weak,nonatomic) IBOutlet UITextField *phoneNum;

@property (weak,nonatomic) IBOutlet UITextField *name;
@property (weak,nonatomic) IBOutlet UITextField *email;
@property (weak,nonatomic) IBOutlet UITextField *date;
@property (weak,nonatomic) IBOutlet UITextField *slot;
@property (weak,nonatomic) IBOutlet UITextField *time;

@property (weak,nonatomic) IBOutlet UIButton *walkin;
@property (weak,nonatomic) IBOutlet UIButton *fix;
@property (weak,nonatomic) IBOutlet UIImageView *bg;

@property (weak,nonatomic) IBOutlet UIView *activityView;

@property (weak,nonatomic ) IBOutlet UIButton *slotDrop;
@property (weak,nonatomic ) IBOutlet UIButton *timeDrop;
@property (weak,nonatomic ) IBOutlet UIButton *nameDrop;

@property NSString *selId;

@end
