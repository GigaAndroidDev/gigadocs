//
//  ClinicAvailabeController.m
//  GigaDocs
//
//  Created by RIR Technologies on 1/2/18.
//  Copyright © 2018 RIR Technologies. All rights reserved.
//

#import "ClinicAvailabeController.h"
#import "LoginViewController.h"
#import "ClinicBookaController.h"
#import "GMDCircleLoader.h"
@interface ClinicAvailabeController ()
{
    NSMutableDictionary *resultDic;
    NSString *selId;
}

@end

@implementation ClinicAvailabeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.bg setImage:[self gradientforview:self.bg.bounds]];

    resultDic=[[NSMutableDictionary alloc]init];
    
    self.activityView.hidden=YES;
    [GMDCircleLoader setOnView:self.activityView withTitle:@"" animated:YES];
    [self getAvailableDoctors];
    
}

-(void)getAvailableDoctors
{
    self.activityView.hidden=NO;
    
    NSString *security=[[NSUserDefaults standardUserDefaults]objectForKey:@"securityKey"];
    NSString *userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    
NSString *myRequestString = [[NSString alloc] initWithFormat:@"user_id=%@&security_key=%@",userid,security];
    
    NSLog(@"my req==%@",myRequestString);
    
    NSData *myRequestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc]initWithURL:[ NSURL URLWithString:@"http://www.gigadocs.com/giga_api/get_doctorsforclinic"]];
    
    [request setHTTPMethod: @"POST"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    [request setHTTPBody: myRequestData];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    
    
    NSURLSessionDataTask * dataTask1 =[defaultSession dataTaskWithRequest:request completionHandler:^(NSData* data, NSURLResponse* response, NSError *error) {                                                          if(error == nil)
        
    {
        
        self.activityView.hidden=YES;
        
        NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        self.activityView.hidden=YES;
        
        
        
        NSLog(@"reslt==%@",result);
        
        NSString *status=[result valueForKey:@"Status"];
        
        if ([status isEqualToString:@"true"])
            
        {
            
            resultDic=[result valueForKey:@"message"];
            NSLog(@"dict==%@",resultDic);
            
        }
        
        else
            
        {
        
            
        }
        
    }
        
        
        
    }];
    
    [dataTask1 resume];

    
}
-(IBAction)slotClicked:(id)sender
{
    if (_doctor.text.length>0)
    {
        ClinicBookaController *book=[[ClinicBookaController alloc]initWithNibName:@"ClinicBookaController" bundle:nil];
        book.selId=selId;
    [self.navigationController pushViewController:book animated:YES];
        
    }
    else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Select Available Doctor" delegate:self cancelButtonTitle: @"OK" otherButtonTitles: nil];
        
        [alert show];
    }
    
}
-(IBAction)dropBtnClikced:(id)sender
{
    
    dropDownView.view.hidden=YES;
    NSArray *arr=[resultDic valueForKey:@"name"];
    
    NSLog(@"arry==%@",arr);

    
    dropDownView = [[DropDownView alloc] initWithArrayData:arr cellHeight:40 heightTableView:40*resultDic.count paddingTop:0 paddingLeft:0 paddingRight:0 refView:self.dropBtn animation:BLENDIN openAnimationDuration:0.5 closeAnimationDuration:0.5];
    dropDownView.view.backgroundColor=[UIColor redColor];
    dropDownView.delegate=self;
    
    [self.view addSubview:dropDownView.view];
    
    self.doctor.text=[[resultDic valueForKey:@"name"] objectAtIndex:0];
    
    
    [dropDownView openAnimation];

}
-(void)dropDownCellSelected:(NSInteger)returnIndex
{
    NSLog(@"hiiii");
    
    
    self.doctor.text =[[resultDic valueForKey:@"name"] objectAtIndex:returnIndex] ;
    
selId =[[resultDic valueForKey:@"id"] objectAtIndex:returnIndex] ;
    dropDownView.view.hidden=YES;
    
}


-(IBAction)logoutClicked:(id)sender
{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Alert"
                                 message:@"Are You Sure want to Logout"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    
                                    LoginViewController *login=[[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
                                    [self.navigationController pushViewController:login animated:YES];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
    {
    [self dismissViewControllerAnimated:alert completion:nil];
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:noButton];
    
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];

   
    
}
-(UIImage*)gradientforview:(CGRect)bounds
{
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:84.0/255.0 green:218.0/255.0 blue:221.0/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:65.0/255.0 green:201.0/255.0 blue:253.0/255.0 alpha:1.0].CGColor];
    
    
    gradient.startPoint=CGPointMake(0.0, 0.0);
    gradient.endPoint = CGPointMake(0.5, 1.0);
    UIGraphicsBeginImageContext(bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *aimage=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return aimage;
}

@end
