//
//  ClinicBookaController.m
//  GigaDocs
//
//  Created by RIR Technologies on 1/1/18.
//  Copyright © 2018 RIR Technologies. All rights reserved.
//

#import "ClinicBookaController.h"
#import "GMDCircleLoader.h"
#import "ClinicAvailabeController.h"
@interface ClinicBookaController ()
{
    NSMutableArray *timeArray,*typeArray,*namesArray;
    
    NSString *type;
    NSString *patinetId;
    NSString *patientType;
    NSString *userType;

}

@end

@implementation ClinicBookaController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.name.delegate=self;
    self.phoneNum.delegate=self;
    self.slot.delegate=self;
    self.email.delegate=self;
    self.date.delegate=self;
    self.time.delegate=self;
    
    timeArray=[[NSMutableArray alloc]init];
    typeArray=[[NSMutableArray alloc]initWithObjects:@"New",@"Follow-Up", nil];
    namesArray=[[NSMutableArray alloc]init];
    
    self.activityView.hidden=YES;
    [GMDCircleLoader setOnView:self.activityView withTitle:@"" animated:YES];
    
    [self.bg setImage:[self gradientforview:self.bg.bounds]];

    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==self.phoneNum)
    {
        //[self getPatientDetails];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (textField==self.date)
    {
        
    
    UIDatePicker *datePicker1 = [[UIDatePicker alloc]init];
    datePicker1.datePickerMode = UIDatePickerModeDate;
    [datePicker1 addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    [self.date setInputView:datePicker1];
    
   

UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,44)];
[toolBar setBarStyle:UIBarStyleBlackOpaque];

UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
UIBarButtonItem *doneButton1 =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(aMethod)];
toolBar.items = [NSArray arrayWithObjects:flexButton,doneButton1, nil];
doneButton1.tintColor=[UIColor whiteColor];

textField.inputAccessoryView=toolBar;
}
else if(textField==self.phoneNum)
{
    
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,44)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    
    UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *doneButton1 =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(aMethod)];
    toolBar.items = [NSArray arrayWithObjects:flexButton,doneButton1, nil];
    doneButton1.tintColor=[UIColor whiteColor];
    
    textField.inputAccessoryView=toolBar;
}
    //7769953319
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField==self.phoneNum)
    {
        
    
    if(range.length + range.location > textField.text.length)
    {
        
        [self.phoneNum resignFirstResponder];
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        if (newLength==10)
        {
            [self getPatientDetails];
        }
        
    return newLength <= 10;
    }
    return true;
}


-(void) dateTextField:(UITextField*)sender
{
    UIDatePicker *picker2 = (UIDatePicker*)sender.inputView;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker2.date;
    [dateFormat setDateFormat:@"dd-MMM-yyyy"];
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    self.date.text = [NSString stringWithFormat:@"%@",dateString];
    
    [self getAvailableSlots];
    
    
}
- (void)aMethod
{
    
    [self.date resignFirstResponder];
    [self.phoneNum resignFirstResponder];
    
}

-(void)getPatientDetails
{
    
//    [self.phoneNum resignFirstResponder];
    self.activityView.hidden=NO;
    
    NSString *security=[[NSUserDefaults standardUserDefaults]objectForKey:@"securityKey"];
    NSString *userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    
    NSString *myRequestString = [[NSString alloc] initWithFormat:@"user_id=%@&security_key=%@&phone_mobile=%@",userid,security,self.phoneNum.text];
    
    NSLog(@"my req==%@",myRequestString);
    
    NSData *myRequestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc]initWithURL:[ NSURL URLWithString:@"http://www.gigadocs.com/giga_api/get_availablepatients"]];
    
    [request setHTTPMethod: @"POST"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    [request setHTTPBody: myRequestData];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    
    
    NSURLSessionDataTask * dataTask1 =[defaultSession dataTaskWithRequest:request completionHandler:^(NSData* data, NSURLResponse* response, NSError *error) {                                                          if(error == nil)
        
    {
        
        self.activityView.hidden=YES;
        
        NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        self.activityView.hidden=YES;
        
        
        
        NSLog(@"reslt==%@",result);
        
        NSString *status=[result valueForKey:@"Status"];
        
        if ([status isEqualToString:@"true"])
            
        {
            namesArray=[[NSMutableArray alloc]init];

            namesArray=[result valueForKey:@"message"];
            
            NSLog(@"namearray==%@",namesArray);
            
            
            if (namesArray.count==0)
            {
                self.nameDrop.hidden=YES;
                
                
            }
            else
            {
                self.name.userInteractionEnabled=false;
                
            }
        }
        
        else
            
        {
            namesArray=[[NSMutableArray alloc]init];
            self.nameDrop.hidden=YES;

            self.name.userInteractionEnabled=true;
            self.name.text=@"";
            self.email.text=@"";

        }
        
    }
    
        
    }];
    
    [dataTask1 resume];
    

}
-(void)getPatientEmail
{
    self.activityView.hidden=NO;
    
    NSString *security=[[NSUserDefaults standardUserDefaults]objectForKey:@"securityKey"];
    
    
    NSString *myRequestString = [[NSString alloc] initWithFormat:@"patient_id=%@&security_key=%@",patinetId,security];
    
    NSLog(@"my req==%@",myRequestString);
    
    NSData *myRequestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc]initWithURL:[ NSURL URLWithString:@"http://www.gigadocs.com/giga_api/get_patientemail"]];
    
    [request setHTTPMethod: @"POST"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    [request setHTTPBody: myRequestData];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    
    
    NSURLSessionDataTask * dataTask1 =[defaultSession dataTaskWithRequest:request completionHandler:^(NSData* data, NSURLResponse* response, NSError *error) {                                                          if(error == nil)
        
    {
        
        self.activityView.hidden=YES;
        
        NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        self.activityView.hidden=YES;
        
        
        
        NSLog(@"reslt==%@",result);
        
        NSString *status=[result valueForKey:@"Status"];
        
        if ([status isEqualToString:@"true"])
            
        {
            self.email.text=[result valueForKey:@"message"];
        }
        
        else
            
        {
            
            self.email.text=@"";
        }
        
    }
        
        
    }];
    
    [dataTask1 resume];
}
-(void)getAvailableSlots
{
    self.activityView.hidden=NO;
    
    NSString *security=[[NSUserDefaults standardUserDefaults]objectForKey:@"securityKey"];
    
    NSString *user=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];

    
    
    NSString *myRequestString = [[NSString alloc] initWithFormat:@"user_id=%@&security_key=%@&doctor_id=%@&appoint_date=%@",user,security,self.selId,self.date.text];
    
    NSLog(@"my req==%@",myRequestString);
    
    NSData *myRequestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc]initWithURL:[ NSURL URLWithString:@"http://www.gigadocs.com/giga_api/get_availableslots"]];
    
    [request setHTTPMethod: @"POST"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    [request setHTTPBody: myRequestData];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    
    
    NSURLSessionDataTask * dataTask1 =[defaultSession dataTaskWithRequest:request completionHandler:^(NSData* data, NSURLResponse* response, NSError *error) {                                                          if(error == nil)
        
    {
        
        self.activityView.hidden=YES;
        
        NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        self.activityView.hidden=YES;
        
        
        
        NSLog(@"reslt==%@",result);
        
        NSString *status=[result valueForKey:@"Status"];
        
        if ([status isEqualToString:@"true"])
            
        {
            timeArray=[result valueForKey:@"message"];
        }
        
        else
            
        {
            
            timeArray=[[NSMutableArray alloc]init];
               UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"No Slots Available For This Date Please Select Another Date" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            
        }
        
    }
        
        
    }];
    
    [dataTask1 resume];
}



-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.name resignFirstResponder];
    [self.phoneNum resignFirstResponder];
    [self.slot resignFirstResponder];
    [self.email resignFirstResponder];
    [self.date resignFirstResponder];
    [self.time resignFirstResponder];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}

-(IBAction)walkinClicked:(id)sender
{
   if ([self.walkin.imageView.image isEqual:[UIImage imageNamed:@"check.png"]])
   {
       [self.walkin setImage:[UIImage imageNamed:@"scheck.png"] forState:UIControlStateNormal];
       
       patientType =@"walkin";
       
       
   }
    else
    {
        [self.walkin setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        
        patientType=@"";

    }
    
}
-(IBAction)bookClicked:(id)sender
{
    if (self.phoneNum.text.length==0 && self.phoneNum.text.length<10)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Please Enter a valid Mobile number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    else if(self.name.text.length==0)

    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Please Enter a Patient Name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if(self.email.text.length==0)
        
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Please Enter a Valid Email Id" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if(self.date.text.length==0)
        
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Please Select Date" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if(self.slot.text.length==0)
        
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Please Select a Slot " delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if(self.time.text.length==0)
        
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Please Select a Type " delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
    
    self.activityView.hidden=NO;
    
    NSString *security=[[NSUserDefaults standardUserDefaults]objectForKey:@"securityKey"];
    NSString *userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];

        NSString *myRequestString;
        
    
        if (namesArray.count==0)
        {
            myRequestString = [[NSString alloc] initWithFormat:@"security_key=%@&user_id=%@&doctor_id=%@&phone_mobile=%@&email_id=%@&date_appoinment=%@&patient_id1=%@&slot_time=%@&followup=%@&walk=%@&status=%@",security,userid,self.selId,self.phoneNum.text,self.email.text,self.date.text,self.name.text,self.slot.text,userType,patientType,@"New"];
        }
    else
    {
        myRequestString = [[NSString alloc] initWithFormat:@"security_key=%@&user_id=%@&doctor_id=%@&phone_mobile=%@&email_id=%@&date_appoinment=%@&patient_id=%@&slot_time=%@&followup=%@&walk=%@",security,userid,self.selId,self.phoneNum.text,self.email.text,self.date.text,patinetId,self.slot.text,userType,patientType];
    }
   
    
    NSLog(@"my req==%@",myRequestString);
    
    NSData *myRequestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc]initWithURL:[ NSURL URLWithString:@"http://www.gigadocs.com/giga_api/add_appoinment"]];
    
    [request setHTTPMethod: @"POST"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    [request setHTTPBody: myRequestData];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    
    
    NSURLSessionDataTask * dataTask1 =[defaultSession dataTaskWithRequest:request completionHandler:^(NSData* data, NSURLResponse* response, NSError *error) {                                                          if(error == nil)
        
    {
        
        self.activityView.hidden=YES;
        
        NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        self.activityView.hidden=YES;
        
        
        
        NSLog(@"reslt==%@",result);
        
        NSString *status=[result valueForKey:@"Status"];
        
        if ([status isEqualToString:@"true"])
            
        {
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Message"
                                         message:@"Appointment Confirmed"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* yesButton = [UIAlertAction
            actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
            {
                                            
                ClinicAvailabeController* clinic=[[ClinicAvailabeController alloc]initWithNibName:@"ClinicAvailabeController" bundle:nil];
                [self.navigationController pushViewController:clinic animated:YES];
            }];
            
           
            
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
        else
            
        {
            
            
        }
        
    }
        
        
    }];
    
    [dataTask1 resume];

    }
    
}
-(IBAction)slotDropClicked:(id)sender
{
    dropDownView.view.hidden=YES;

    
    if (timeArray.count>0)
    {
        dropDownView = [[DropDownView alloc] initWithArrayData:timeArray cellHeight:40 heightTableView:40*timeArray.count paddingTop:0 paddingLeft:0 paddingRight:0 refView:self.slotDrop animation:BLENDIN openAnimationDuration:0.5 closeAnimationDuration:0.5];
        dropDownView.view.backgroundColor=[UIColor redColor];
        dropDownView.delegate=self;
        
        [self.view addSubview:dropDownView.view];
        
        self.slot.text=[timeArray objectAtIndex:0];
        
        type=@"slot";
        
        
        [dropDownView openAnimation];

    }
    

}
-(IBAction)nameDropClicked:(id)sender
{
    
    dropDownView.view.hidden=YES;

    
    NSArray *Arr=[namesArray valueForKey:@"name"];
    
    NSLog(@"ay===%@",Arr);
    
    if (namesArray.count >0)
    {
        dropDownView = [[DropDownView alloc] initWithArrayData:Arr cellHeight:40 heightTableView:40*namesArray.count paddingTop:0 paddingLeft:0 paddingRight:0 refView:self.nameDrop animation:BLENDIN openAnimationDuration:0.5 closeAnimationDuration:0.5];
        dropDownView.view.backgroundColor=[UIColor redColor];
        dropDownView.delegate=self;
        
        [self.view addSubview:dropDownView.view];
        
        self.name.text=[[namesArray valueForKey:@"name"] objectAtIndex:0];
        
        
        type=@"name";
        
        [dropDownView openAnimation];

    }
    
    
}
-(IBAction)timeDropClciked:(id)sender
{
    
    dropDownView.view.hidden=YES;

    
    if (typeArray.count>0)
    {
        dropDownView = [[DropDownView alloc] initWithArrayData:typeArray cellHeight:40 heightTableView:40*typeArray.count paddingTop:0 paddingLeft:0 paddingRight:0 refView:self.timeDrop animation:BLENDIN openAnimationDuration:0.5 closeAnimationDuration:0.5];
        dropDownView.view.backgroundColor=[UIColor redColor];
        dropDownView.delegate=self;
        
        [self.view addSubview:dropDownView.view];
        
        self.time.text=[typeArray objectAtIndex:0];
        
        type=@"time";
        
        
        [dropDownView openAnimation];

    }
    
    

}
-(void)dropDownCellSelected:(NSInteger)returnIndex
{
    NSLog(@"hiiii");
    
    if ([type isEqualToString:@"name"])
    {
        self.name.text =[[namesArray valueForKey:@"name"]objectAtIndex:returnIndex] ;
        patinetId=[[namesArray valueForKey:@"id"]objectAtIndex:returnIndex] ;
        [self getPatientEmail];
    }
    else if([type isEqualToString:@"slot"])
    {
        
        self.slot.text =[timeArray objectAtIndex:returnIndex] ;
    }
    else
    {
        self.time.text =[typeArray objectAtIndex:returnIndex] ;
        
        if (returnIndex ==0)
        {
            userType=@"new";
        }
        else{
            userType=@"followup";

        }

    }
    
    
    dropDownView.view.hidden=YES;
    
}

-(UIImage*)gradientforview:(CGRect)bounds
{
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:84.0/255.0 green:218.0/255.0 blue:221.0/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:65.0/255.0 green:201.0/255.0 blue:253.0/255.0 alpha:1.0].CGColor];
    
    
    gradient.startPoint=CGPointMake(0.0, 0.0);
    gradient.endPoint = CGPointMake(0.5, 1.0);
    UIGraphicsBeginImageContext(bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *aimage=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return aimage;
}
-(IBAction)backClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
