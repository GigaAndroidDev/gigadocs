//
//  MenuCellCell.h
//  GigaDocs
//
//  Created by RIR Technologies on 12/21/17.
//  Copyright © 2017 RIR Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCellCell : UITableViewCell

@property (weak,nonatomic) IBOutlet UILabel *titleL;
@property (weak,nonatomic) IBOutlet UIImageView *icon;


@end
