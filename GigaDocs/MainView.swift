//
//  MainView.swift
//  Handwriting
//
//  Created by Collin Hundley on 5/1/17.
//  Copyright © 2017 Swift AI. All rights reserved.
//

import UIKit


class MainView: UIView {
    
    // Nav bar
    var navBar = UIView()
    // Canvas
    var canvasContainer = ShadowView()
    let canvas = UIImageView()
    var snapshotBox = UIView()
    
    var bottomView = UIView()

    
    
    
    // Input
    // Output
    
    convenience init() {
        self.init(frame: .zero)
        

        
        configureSubviews()
        configureLayout()
    }
    
    /// Configure view/subview appearances.
    private func configureSubviews() {
        backgroundColor = UIColor.saiExtraLightGray()
        
        // Nav bar
        navBar.backgroundColor = .clear
        
        
        
        
        // Snapshot box
        
        snapshotBox.backgroundColor = .clear
        snapshotBox.layer.borderColor = UIColor.saiGreen().cgColor
        snapshotBox.layer.borderWidth = 2
        snapshotBox.layer.cornerRadius = 3
        
        // Output title
        
    }
    
    /// Add subviews and set constraints.
    private func configureLayout()
    
    
    {
        
    
        
        
    
        addAutoLayoutSubviews(navBar, canvasContainer,bottomView)
        
        canvasContainer.addAutoLayoutSubviews(canvas, snapshotBox,bottomView)
        
        
        

        NSLayoutConstraint.activate([
            navBar.leftAnchor.constraint(equalTo: leftAnchor),
            navBar.rightAnchor.constraint(equalTo: rightAnchor),
            navBar.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            navBar.heightAnchor.constraint(equalToConstant: 0)
            ])
        
        
        
        NSLayoutConstraint.activate([
            canvasContainer.leftAnchor.constraint(equalTo: leftAnchor, constant: 5),
            canvasContainer.rightAnchor.constraint(equalTo: rightAnchor, constant: 15),
            canvasContainer.topAnchor.constraint(equalTo: navBar.bottomAnchor, constant: 65),
            canvasContainer.bottomAnchor.constraint(equalTo: bottomAnchor , constant: 65)
            ])
        
        
//       
//        NSLayoutConstraint.activate([
//            bottomView.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
//            bottomView.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
//            bottomView.bottomAnchor.constraint(equalTo: bottomAnchor),
//            bottomView.heightAnchor.constraint(equalToConstant: 50)
//            ])
        


        
//
        // Canvas
        canvas.fillSuperview()
        
        
    }
    
     override func fillSuperview()
    {
        guard let superview = self.superview else { return }
        NSLayoutConstraint.activate([
            leftAnchor.constraint(equalTo: superview.leftAnchor, constant: 0),
            rightAnchor.constraint(equalTo: superview.rightAnchor, constant: 0),
            topAnchor.constraint(equalTo: superview.topAnchor, constant: 30),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: 80)
            ])
    }
    

}
