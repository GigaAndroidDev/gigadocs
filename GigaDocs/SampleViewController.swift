//
//  SampleViewController.swift
//  GigaDocs
//
//  Created by RIR Technologies on 12/31/17.
//  Copyright © 2017 RIR Technologies. All rights reserved.
//

import UIKit


@objc public class SampleViewController: UIViewController {

    @IBOutlet var activity: UIView!
    // View
    let mainView = MainView()
    
    var imageView = UIImageView()
    var labelOut = UILabel()
    
    public var outString : String = ""
    

  public  var  dictionary = [AnyHashable: Any]()

    @IBOutlet var view1: UIView!
    // Neural network
    var neuralNet: NeuralNet!
    
    // Drawing state variables
    /// The sketch brush width.
    fileprivate let brushWidth: CGFloat = 2
    fileprivate var lastDrawPoint = CGPoint.zero
    fileprivate var boundingBox: CGRect?
    fileprivate var hasSwiped = false
    /// Flag designating whether the user is currently in the process of drawing.
    fileprivate var isDrawing = false
    /// Timer used for snapshotting the sketch.
    fileprivate var timer = Timer()
    
    override public func loadView()
    {
        self.view = mainView
        
        let back=UIButton(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 60))
        back.backgroundColor = .red
        self.view .addSubview(back)
    }
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialize neural network
        do {
            guard let url = Bundle.main.url(forResource: "neuralnet-mnist-trained", withExtension: nil) else {
                fatalError("Unable to locate trained neural network file in bundle.")
            }
            neuralNet = try NeuralNet(url: url)
        } catch {
            fatalError("\(error)")
        }
        
        let theHeight = UIScreen.main.bounds.size.height //grabs the height of your view

        
        let lable=UILabel(frame: CGRect(x: 0, y: theHeight-60, width: UIScreen.main.bounds.size.width, height: 60))
            lable.backgroundColor = .cyan
        self.view .addSubview(lable)
        
        
        
        
        
        let backImg=UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 60))
        backImg.backgroundColor = .cyan
        self.view .addSubview(backImg)
        
       
        
        
        
        
        let back=UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        back.setTitle("< back", for: .normal)
        back.addTarget(self, action: #selector(backClicked), for: .touchUpInside)
        self.view .addSubview(back)
        
        
        let image = UIImage(named: "check") as UIImage?


        
        let save1=UIButton(frame: CGRect(x: 0, y: theHeight-60, width:UIScreen.main.bounds.size.width/3,  height: 30))
        save1.addTarget(self, action: #selector(saveClicked), for: .touchUpInside)
        save1.setImage(image, for: .normal)
        
        self.view .addSubview(save1)

        
        
        let save2=UILabel(frame: CGRect(x: 0, y: theHeight-30, width:UIScreen.main.bounds.size.width/3,  height: 30))
        save2.text="Save/Send"
        save2.textAlignment = .center
        save2.font=UIFont.systemFont(ofSize: 10)

        self.view .addSubview(save2)
        
        

        

        let save=UIButton(frame: CGRect(x: 0, y: theHeight-60, width:UIScreen.main.bounds.size.width/3,  height: 60))
        save.addTarget(self, action: #selector(saveClicked), for: .touchUpInside)

        self.view .addSubview(save)

        
        let active1=UIButton(frame: CGRect(x: UIScreen.main.bounds.size.width/3, y: theHeight-60, width: UIScreen.main.bounds.size.width/3, height: 30))
        active1.addTarget(self, action: #selector(activeClicked), for: .touchUpInside)
        active1.setImage(image, for: .normal)
        
        self.view .addSubview(active1)
        
        let active2=UILabel(frame: CGRect(x: UIScreen.main.bounds.size.width/3, y: theHeight-30, width:UIScreen.main.bounds.size.width/3,  height: 30))
        active2.text="Accept"
        active2.textAlignment = .center
        active2.font=UIFont.systemFont(ofSize: 10)
        
        self.view .addSubview(active2)

        
        let active=UIButton(frame: CGRect(x: UIScreen.main.bounds.size.width/3, y: theHeight-60, width: UIScreen.main.bounds.size.width/3, height: 60))
        active.addTarget(self, action: #selector(activeClicked), for: .touchUpInside)

        self.view .addSubview(active)
        
        
        let clear1=UIButton(frame: CGRect(x: active.frame.origin.x+UIScreen.main.bounds.size.width/3, y: theHeight-60, width: UIScreen.main.bounds.size.width/3, height: 30))
        clear1.addTarget(self, action: #selector(clearClicked), for: .touchUpInside)
        clear1.setImage(image, for: .normal)
        self.view .addSubview(clear1)

        let clear2=UILabel(frame: CGRect(x: active.frame.origin.x+UIScreen.main.bounds.size.width/3, y: theHeight-30, width:UIScreen.main.bounds.size.width/3,  height: 30))
        clear2.text="Clear"
        clear2.textAlignment = .center
        clear2.font=UIFont.systemFont(ofSize: 10)
        self.view .addSubview(clear2)
        

        
        
        let clear=UIButton(frame: CGRect(x: active.frame.origin.x+UIScreen.main.bounds.size.width/3, y: theHeight-60, width: UIScreen.main.bounds.size.width/3, height: 60))
        clear.addTarget(self, action: #selector(clearClicked), for: .touchUpInside)
        

        self.view .addSubview(clear)


        
        imageView=UIImageView(frame: CGRect(x: 0, y: 60, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height-120))
        imageView.backgroundColor = .cyan
        self.view .addSubview(imageView)
        
        imageView.isHidden=true
        
        
        labelOut = UILabel(frame: CGRect(x: 30, y: theHeight-110, width:UIScreen.main.bounds.size.width,  height: 50))
        
            labelOut.text = outString
        labelOut.numberOfLines=0
        
        labelOut.font=UIFont.systemFont(ofSize: 8)
        
        self.view .addSubview(labelOut)

        
        
    }
    func backClicked(sender: UIButton!)
    {

//        let sample:DoctorHomeController = DoctorHomeController(nibName:"DoctorHomeController", Bundle:nil)
        
        
        let sample = DoctorHomeController(nibName: "DoctorHomeController", bundle: nil)
        
        
        self.navigationController?.pushViewController(sample, animated: false)
        
//        self.navigationController?.popViewController(animated: true)
     
    }
    
    func clearClicked(sender: UIButton!)
    {
//        self.loadView()
//
//        self.viewDidLoad()
//        
//        clearCanvas()
        
        
        let sample = SampleViewController(nibName: "SampleViewController", bundle: nil)
        sample.dictionary = self.dictionary
        sample.outString = self.outString
        
        self.navigationController?.pushViewController(sample, animated: false)
        
        
    }
    func saveClicked()
    {
        
      
       
        
        let screenRect: CGRect =  CGRect(x: 0, y: 60, width:   UIScreen.main.bounds.width, height:   UIScreen.main.bounds.height-120)
        
        UIGraphicsBeginImageContext(screenRect.size)
        let ctx: CGContext? = UIGraphicsGetCurrentContext()
        UIColor.black.set()
        ctx?.fill(screenRect)
        // grab reference to our window
        let window: UIView? = mainView
        // transfer content into our context
        window?.layer.render(in: ctx!)
        let screengrab: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        imageView.isHidden=false
        imageView.image=screengrab
//        mainView.isHidden=true
        crop(image: screengrab!)
        
    }
    func crop(image:UIImage)
    {
        let _:CGFloat = 1
        let x:CGFloat = 0
        let y:CGFloat = 60
        let width:CGFloat = UIScreen.main.bounds.width
        let height:CGFloat = UIScreen.main.bounds.height-120
        let croppedCGImage = imageView.image?.cgImage?.cropping(to: CGRect(x: x, y: y, width: width, height: height))
        let croppedImage = UIImage(cgImage: croppedCGImage!)
        
        imageView.image=croppedImage

        
        
//        var base64String = imgData.base64EncodedStringWithOptions(NSData.Base64EncodingOptions.fromRaw(0)!) // encode the image

        
        profileUpload(img:croppedImage )
        
    }
    func activeClicked(sender: UIButton!)
    {
        
    }

    
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    

}

// MARK: Touch handling

extension SampleViewController {
    
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        
        // Reset swipe state tracker
        hasSwiped = false
        
        // Make sure the touch is inside the canvas
        guard mainView.canvasContainer.frame.contains(touch.location(in: mainView)) else {
            return super.touchesBegan(touches, with: event)
        }
        
        // Determine touch point
        let location = touch.location(in: mainView.canvas)
        
        // Reset bounding box if needed
        if boundingBox == nil {
            boundingBox = CGRect(x: location.x - brushWidth / 2,
                                 y: location.y - brushWidth / 2,
                                 width: brushWidth, height: brushWidth)
        }
        
        // Store draw location
        lastDrawPoint = location
        
        // Set drawing flag
        isDrawing = true
        
        // Invalidate timer
        timer.invalidate()
    }
    
    override public func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        guard let touch = touches.first else { return }
        
        // Make sure the touch is inside the canvas
        guard mainView.canvasContainer.frame.contains(touch.location(in: mainView)) else {
            hasSwiped = false
            return super.touchesMoved(touches, with: event)
        }
        
        // Determine touch point
        let currentPoint = touch.location(in: mainView.canvas)
        
        // Reset bounding box if needed
        if boundingBox == nil {
            boundingBox = CGRect(x: currentPoint.x - brushWidth,
                                 y: currentPoint.y - brushWidth,
                                 width: brushWidth, height: brushWidth)
        }
        
        // Draw a line from previous to current touch point
        if hasSwiped {
            drawLine(from: lastDrawPoint, to: currentPoint)
        } else {
            drawLine(from: currentPoint, to: currentPoint)
            hasSwiped = true
        }
        
        // Expand the bounding box to fit the extremes of the sketch
        if currentPoint.x < boundingBox!.minX {
            stretchBoundingBox(minX: currentPoint.x - brushWidth,
                               maxX: nil, minY: nil, maxY: nil)
        } else if currentPoint.x > boundingBox!.maxX {
            stretchBoundingBox(minX: nil,
                               maxX: currentPoint.x + brushWidth,
                               minY: nil, maxY: nil)
        }
        
        if currentPoint.y < boundingBox!.minY {
            stretchBoundingBox(minX: nil, maxX: nil,
                               minY: currentPoint.y - brushWidth,
                               maxY: nil)
        } else if currentPoint.y > boundingBox!.maxY {
            stretchBoundingBox(minX: nil, maxX: nil, minY: nil,
                               maxY: currentPoint.y + brushWidth)
        }
        
        // Store draw location
        lastDrawPoint = currentPoint
        
        // Invalidate timer
        timer.invalidate()
    }
    
    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        
        // Make sure touch is inside canvas
        if mainView.canvasContainer.frame.contains(touch.location(in: mainView)) {
            if !hasSwiped {
                // Draw dot
                drawLine(from: lastDrawPoint, to: lastDrawPoint)
            }
        }
        
        // Start timer
        if #available(iOS 10.0, *) {
            timer = Timer.scheduledTimer(withTimeInterval: 0.4, repeats: false) { [weak self] (_) in
                self?.timerExpired()
            }
        } else {
            // Fallback on earlier versions
        }
        
        // We're no longer drawing
        isDrawing = false
        
        super.touchesEnded(touches, with: event)
    }
    
}


// MARK: Drawing and image manipulation


extension SampleViewController {
    
    /// Draws a line on the canvas between the given points.
    fileprivate func drawLine(from: CGPoint, to: CGPoint) {
        // Begin graphics context
        UIGraphicsBeginImageContext(mainView.canvas.bounds.size)
        let context = UIGraphicsGetCurrentContext()
        
        // Store current sketch in context
        mainView.canvas.image?.draw(in: mainView.canvas.bounds)
        
        // Append new line to image
        context?.move(to: from)
        context?.addLine(to: to)
        context?.setLineCap(CGLineCap.round)
        context?.setLineWidth(brushWidth)
        context?.setStrokeColor(red: 0, green: 0, blue: 0, alpha: 1)
        context?.strokePath()
        
        // Store modified image back into image view
        mainView.canvas.image = UIGraphicsGetImageFromCurrentImageContext()
        
        // End context
        UIGraphicsEndImageContext()
    }
    
    /// Crops the given UIImage to the provided CGRect.
    fileprivate func crop(_ image: UIImage, to: CGRect) -> UIImage {
        let img = image.cgImage!.cropping(to: to)
        return UIImage(cgImage: img!)
    }
    
    /// Scales the given image to the provided size.
    fileprivate func scale(_ image: UIImage, to: CGSize) -> UIImage {
        let size = CGSize(width: min(20 * image.size.width / image.size.height, 20),
                          height: min(20 * image.size.height / image.size.width, 20))
        let newRect = CGRect(x: 0, y: 0, width: size.width, height: size.height).integral
        UIGraphicsBeginImageContextWithOptions(size, false, 1.0)
        let context = UIGraphicsGetCurrentContext()
        context?.interpolationQuality = .none
        image.draw(in: newRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    /// Centers the given image in a clear 28x28 canvas and returns the result.
    fileprivate func addBorder(to image: UIImage) -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 28, height: 28))
        image.draw(at: CGPoint(x: (28 - image.size.width) / 2,
                               y: (28 - image.size.height) / 2))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    /// Updates the bounding box to stretch to the provided extremes.
    /// If `nil` is passed for any value, the box's current value will be preserved.
    fileprivate func stretchBoundingBox(minX: CGFloat?, maxX: CGFloat?, minY: CGFloat?, maxY: CGFloat?) {
        guard let box = boundingBox else { return }
        boundingBox = CGRect(x: minX ?? box.minX,
                             y: minY ?? box.minY,
                             width: (maxX ?? box.maxX) - (minX ?? box.minX),
                             height: (maxY ?? box.maxY) - (minY ?? box.minY))
    }
    
    /// Resets the canvas for a new sketch.
    fileprivate func clearCanvas() {
        // Animate snapshot box
        if boundingBox != nil
        {
            mainView.snapshotBox.transform = CGAffineTransform(scaleX: 0.96, y: 0.96)
            // Spring outward
            UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: [], animations: {
                self.mainView.snapshotBox.alpha = 1
                self.mainView.snapshotBox.transform = CGAffineTransform(scaleX: 1.06, y: 1.06)
            }, completion: nil)
            // Spring back inward
            UIView.animate(withDuration: 0.3, delay: 0.1, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: [], animations: {
                self.mainView.snapshotBox.transform = CGAffineTransform.identity
            }, completion: nil)
        }
        
        // Animate the sketch and bounding box away
        
    }
    
}


// MARK: Classification

extension SampleViewController {
    
    /// Called when the timer expires after the user stops drawing.
    fileprivate func timerExpired() {
        // Perform classification
        classifyImage()
        // Reset bounding box
        boundingBox = nil
    }
    
    /// Attempts to classify the current sketch, displays the result, and clears the canvas.
    private func classifyImage() {
        // Clear canvas when finished
        defer { clearCanvas() }
        
        // Extract and resize image from drawing canvas
        guard let imageArray = scanImage() else { return }
        
        // Perform classification
        do {
            _ = try neuralNet.infer(imageArray)
//            if let (label, confidence) = label(from: output)
//            {
//                //                displayOutputLabel(label: label, confidence: confidence)
//            } else {
//                //                mainView.outputLabel.text = "Err"
//            }
        } catch {
            print(error)
        }
        
        // Clear the canvas
        clearCanvas()
    }
    
    /// Scans the current image from the canvas and returns the pixel data as Floats.
    private func scanImage() -> [Float]? {
        var pixelsArray = [Float]()
        guard let image = mainView.canvas.image, let box = boundingBox else {
            return nil
        }
        
        // Extract drawing from canvas and remove surrounding whitespace
        let croppedImage = crop(image, to: box)
        
        // Scale sketch to max 20px in both dimmensions
        let scaledImage = scale(croppedImage, to: CGSize(width: 20, height: 20))
        
        // Center sketch in 28x28 white box
        let character = addBorder(to: scaledImage)
        
        // Dispaly character in view
       // mainView.networkInputCanvas.image = character
        
        // Extract pixel data from scaled/cropped image
        guard let cgImage = character.cgImage else { return nil }
        guard let pixelData = cgImage.dataProvider?.data else { return nil }
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
        let bytesPerRow = cgImage.bytesPerRow
        let bytesPerPixel = cgImage.bitsPerPixel / 8
        
        // Iterate through
        var position = 0
        for _ in 0..<Int(character.size.height) {
            for _ in 0..<Int(character.size.width) {
                // We only care about the alpha component
                let alpha = Float(data[position + 3])
                // Scale alpha down to range [0, 1] and append to array
                pixelsArray.append(alpha / 255)
                // Increment position
                position += bytesPerPixel
            }
            if position % bytesPerRow != 0 {
                position += (bytesPerRow - (position % bytesPerRow))
            }
        }
        return pixelsArray
    }
    
    /// Extracts the output integer and confidence from the given neural network output.
    private func label(from output: [Float]) -> (label: Int, confidence: Float)? {
        guard let max = output.max() else { return nil }
        return (output.index(of: max)!, max)
    }
    func profileUpload(img: UIImage)
        
    {
        
        let imgData: NSData = NSData(data: UIImageJPEGRepresentation((img), 1)!)
        
        let imageSize: Int = imgData.length
        print("size of image in KB: %f ", Double(imageSize) / 1024.0)
//        let base64String = imgData.base64EncodedString(options:[])
        
    
        let imageupload =  ImageFileUpload()
        imageupload.dictionary=self.dictionary
        
      let string =  imageupload.imageupload(img, accept: "1")
        
        
        if string == "true"
            
        {
            let sample = DoctorHomeController(nibName: "DoctorHomeController", bundle: nil)
        
        self.navigationController?.pushViewController(sample, animated: false)
        }

        
//        let security = UserDefaults.standard.object(forKey: "securityKey") as? String
//        ////        _ = UserDefaults.standard.object(forKey: "userid") as? String
//           let hospitalid = UserDefaults.standard.object(forKey: "hospoita_id") as? String
//        ////        var _params = [AnyHashable: Any]()
//        ////        _params["security_key"] = security
//        ////        _params["patient_accept"] = "1"
//        ////        _params["appoint_id"] = dictionary["apo_id"]
//        ////        _params["medicine"] = ""
//        ////        _params["user_id"] = hospitalid
//        ////        _params["patient_id"] = dictionary["patient_id"]
//        ////        _params["camera_status"] = "1"
//        ////        _params["doctor_id"] = dictionary["doctor_id"]
//        ////        
//
//        
//        
//
//        
//        let dict = ["security_key":security,"patient_accept":"1","appoint_id":dictionary["apo_id"] , "medicine":"","user_id":hospitalid, "patient_id": dictionary["patient_id"],"camera_status":"0","doctor_id":dictionary["doctor_id"],"attach":base64String] as Dictionary<String, AnyObject>
//        
//        
//        
//        
//        
//        let jsonData: NSData = try!  JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
//        
//        
//        
//        _ = NSString(data: jsonData as Data,encoding: String.Encoding.ascii.rawValue)
//        
//        
//        
//     
//        
//        
//        print(dict)
//        
//    
//        
//        // creating the request
//        
//        
//       
//        
//        
//        
//        let url = NSURL(string: "http://www.gigadocs.com/test/giga_api/addprescription")
//        
//        
//        
//        print(url as Any)
//        
//        
//        
//        
//        
//        
//        
//        let request = NSMutableURLRequest(url: url! as URL)
//        
//        request.httpMethod = "POST"
//        
//        
//        
//        
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
//        
//        request.addValue("json", forHTTPHeaderField: "Data-Type")
//        
//        
//        
//        request.setValue("(UInt(jsonData.length))", forHTTPHeaderField: "Content-Length")
//        
//        request .httpBody = jsonData as Data
//        
//        
//        
//        let session = URLSession.shared
//        
//        
//        
//        let dataTask = session.dataTask(with: request as URLRequest) {
//            
//            (data, response, error) in
//            
//            
//            
//            DispatchQueue.main.async()
//                
//                {
//                    
//                    
//                    
//                    
//                    
//            }
//            
//            
//            
//            if((error) != nil)
//                
//            {
//                
//                print(error!.localizedDescription)
//                
//                
//                
//                
//                
//            }
//                
//                
//                
//            else
//                
//            {
//                
//                _ = NSString(data: data!, encoding:String.Encoding.utf8.rawValue)
//                
//                
//                
//                let _: NSError?
//                
//                
//                
//                let jsonResult = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! NSDictionary
//                
//                
//                
//                print(jsonResult)
//                
//                
//                
//                        
//                
//                
//                
//            }
//            
//        }
//        
//        dataTask.resume()
   
        
        
        
        
//        let url = NSURL(string: "http://www.gigadocs.com/test/giga_api/addprescription")
//        
//        
//        
//        print(url as Any)
//        
//        
//        
////        loader.startAnimating()
////        
////        activityView.isHidden = false
////        
//        
//        
////        let request = NSMutableURLRequest(url: url! as URL)
////        
////        
////        
////        request.httpMethod="POST"
//        
//
//        
//       let security = UserDefaults.standard.object(forKey: "securityKey") as? String
////        _ = UserDefaults.standard.object(forKey: "userid") as? String
//        let hospitalid = UserDefaults.standard.object(forKey: "hospoita_id") as? String
////        var _params = [AnyHashable: Any]()
////        _params["security_key"] = security
////        _params["patient_accept"] = "1"
////        _params["appoint_id"] = dictionary["apo_id"]
////        _params["medicine"] = ""
////        _params["user_id"] = hospitalid
////        _params["patient_id"] = dictionary["patient_id"]
////        _params["camera_status"] = "1"
////        _params["doctor_id"] = dictionary["doctor_id"]
////        
////        print(_params)
////        
//     
//        let myUrl = NSURL(string: "http://www.gigadocs.com/test/giga_api/addprescription");
//        
//        let request = NSMutableURLRequest(url:myUrl! as URL);
//        request.httpMethod = "POST";
//        
//        let param = [
//            "security_key"  : security,
//            "patient_accept"    : "1",
//            "appoint_id"    : dictionary["apo_id"],
//            "medicine"      :"",
//            "user_id"       :hospitalid,
//            "patient_id"    :dictionary["patient_id"],
//            "camera_status"  :"0",
//            "doctor_id"       :dictionary["doctor_id"]
//        ]
//        
//        let boundary = generateBoundaryString()
//        
//        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//        
//        
//        let imageData = UIImageJPEGRepresentation(img, 1)
//        
//        if(imageData==nil)  { return; }
//        
//        request.httpBody = createBodyWithParameters(parameters: param as? [String : String], filePathKey: "attach", imageDataKey: imageData! as NSData, boundary: boundary) as Data
//        
//        
////        myActivityIndicator.startAnimating();
//        
//       let task = URLSession.shared.dataTask(with: request as URLRequest) {
//            data, response, error in
//            
//            if error != nil {
//                print("error=\(String(describing: error))")
//                return
//            }
//            
//            // You can print out response object
//            print("******* response = \(String(describing: response))")
//            
//            // Print out reponse body
//            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
//            print("****** response data = \(responseString!)")
//            
//            do {
//                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
//                
//                print(json )
//                
//                DispatchQueue.main.async(execute: {
////                    self.myActivityIndicator.stopAnimating()
////                    self.myImageView.image = nil;
//                });
//                
//            }catch
//            {
//                print(error)
//            }
//            
//        }
//        
//        task.resume()
//        
//        
//        
////        
////        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
////        
////        
////        
////        let imageData = UIImageJPEGRepresentation(img, 0.1)
////        
////        
////        
////        if imageData==nil
////            
////        {
////            
////            print("image data is nil")
////            
////            
////            
////            return
////            
////            
////            
////        }
////        
////        
////        
////        request.httpBody = createBodyWithParameters(parameters: _params as? [String : String], filePathKey:"attach", imageDataKey: imageData! as NSData, boundary: boundary) as Data
////        
////        
////        
////        let task = URLSession.shared.dataTask(with: request as URLRequest)
////            
////        {
////            
////            data, response, error in
////            
////            
////            
////            
////            
////            DispatchQueue.main.async()
////                
////                {
////                    
////                    
////                    
//////                    self.loader.stopAnimating()
//////                    
//////                    self.loader.isHidden = true
//////                    
//////                    self.activityView.isHidden = true
//////                    
////                    
////                    
////            }
////            
////            
////
////            if error != nil
////                
////            {
////                
////                print("error=\(String(describing: error))")
////                
////                return
////                
////            }
////            
////            
////            
////            //You can print out response object
////            
////            print("***** response = \(String(describing: response))")
////            
////            
////            
////            // Print out reponse body
////            
////            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
////            
////            print("**** response data = \(responseString!)")
////            
////            
////            
////            do {
////                
////                
////                
////                let json = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! NSDictionary
////                
////                
////                
////                
////                
////                DispatchQueue.main.async()
////                    
////                    {
////                        
////            print(NSString(data: data!, encoding:String.Encoding.utf8.rawValue)!)
////                        
////            print(json)
////                        
////                        
////                        let staus = json["Status"] as! String
////               
////                        if staus == "true"
////                        {
////                            let sample = DoctorHomeController(nibName: "DoctorHomeController", bundle: nil)
////                            
////                            
////                            self.navigationController?.pushViewController(sample, animated: false)                        }
////                        
////                        
////                        
////                }
////                
////            }
////        }
////        
////        task.resume()
        
    }

    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData
        
    {
        
        
        
        let body = NSMutableData()
        
        let tempData = NSMutableData()
        
        for (key, value) in parameters! {
            
            tempData.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            
            tempData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            
            tempData.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
            
            // etc etc
            
        }
        
        
        
        body.append(tempData as Data)
        
        
        
        let filename = String(format: "hgfvcdsxza", "%@")
        
        let mimetype = "image/jpg"
        
        
        
        
        
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        
        body.append("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n".data(using: String.Encoding.utf8)!)
        
        body.append("Content Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
        
        body.append(imageDataKey as Data)
        
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        
        
        
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        
        
        
        return body
        
        
        
    }
    
    
    
    func generateBoundaryString() -> String
        
    {
        
        return "----------V2ymHFg03ehbqgZCaKO6jy"
        
    }
    
}
extension NSMutableData
    
{
    
    func append(string: String)
        
    {
        
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        append(data!)
        
        
        
    }
    
}

