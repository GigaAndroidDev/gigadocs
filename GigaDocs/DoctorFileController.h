//
//  DoctorFileController.h
//  GigaDocs
//
//  Created by RIR Technologies on 12/21/17.
//  Copyright © 2017 RIR Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoctorFileController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIImagePickerController*cameraPic;
    UIImagePickerController*photoGalleryImage;

}
@property (weak,nonatomic) IBOutlet UIImageView *background;
@property (weak,nonatomic) IBOutlet UIButton *accept;
@property (weak,nonatomic) IBOutlet UIButton *save;
@property (weak,nonatomic) IBOutlet UIButton *clear;
@property  (weak,nonatomic) IBOutlet UILabel *patientName;
@property  (weak,nonatomic) IBOutlet UILabel *phoneNum;
@property  (weak,nonatomic) IBOutlet UILabel *patientAddress;
@property  (weak,nonatomic) IBOutlet UILabel *emailId;
@property  (weak,nonatomic) IBOutlet UIImageView *cameraImage;
@property (weak,nonatomic) IBOutlet UIView *activityView;

@property NSDictionary *dictionary;

@end
