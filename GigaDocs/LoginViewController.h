//
//  LoginViewController.h
//  GigaDocs
//
//  Created by RIR Technologies on 12/21/17.
//  Copyright © 2017 RIR Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate>

@property (weak,nonatomic) IBOutlet UITextField *emailTf;
@property (weak,nonatomic) IBOutlet UITextField *passwordTf;
@property (weak,nonatomic) IBOutlet UIButton *signup;
@property (weak,nonatomic) IBOutlet UIButton *login;
@property (weak,nonatomic) IBOutlet UIButton *remeber;
@property (weak,nonatomic) IBOutlet UIButton *doctor;
@property (weak,nonatomic) IBOutlet UIImageView *background;
@property (weak,nonatomic) IBOutlet UIView *activityView;

@property (weak,nonatomic) IBOutlet UIButton *hospital;
@property (weak,nonatomic) IBOutlet UIButton *patient;

@end
