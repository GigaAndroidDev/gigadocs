//
//  RegisterViewController.m
//  GigaDocs
//
//  Created by RIR Technologies on 12/21/17.
//  Copyright © 2017 RIR Technologies. All rights reserved.
//

#import "RegisterViewController.h"
#import "LoginViewController.h"
#import "DoctorHomeController.h"
#import "GMDCircleLoader.h"
@interface RegisterViewController ()
{
    NSString *userType;
    NSString *user;

}

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.nameTf.delegate=self;
    self.emailTf.delegate=self;
    self.mobileTf.delegate=self;
    self.passwordTf.delegate=self;
    self.licenseTf.delegate=self;
    
    self.licenseTf.hidden=NO;
    
    userType=@"DOCTOR";
  user=@"YES";
    
    self.login.clipsToBounds=YES;
    self.login.layer.cornerRadius=1.0f;
    self.login.layer.borderWidth=1.0f;
    self.login.layer.borderColor=[UIColor colorWithRed:52.0/255.0 green:181.0/255.0 blue:236.0/255.0 alpha:1.0f].CGColor;
    
    self.activityView.hidden=YES;
    [GMDCircleLoader setOnView:self.activityView withTitle:@"" animated:YES];

    
    self.signup.clipsToBounds=YES;
    self.signup.layer.cornerRadius=1.0f;
    self.signup.layer.borderWidth=1.0f;
    self.signup.layer.borderColor=[UIColor colorWithRed:52.0/255.0 green:181.0/255.0 blue:236.0/255.0 alpha:1.0f].CGColor;
    
    
    self.passwordTf.rightViewMode = UITextFieldViewModeAlways;
    self.passwordTf.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password.png"]];
    
    
    
    self.emailTf.rightViewMode = UITextFieldViewModeAlways;
    self.emailTf.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mail.png"]];
    
    
    self.nameTf.rightViewMode = UITextFieldViewModeAlways;
    self.nameTf.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user.png"]];
    
    
    
    self.licenseTf.rightViewMode = UITextFieldViewModeAlways;
    self.licenseTf.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mail.png"]];
    
    
    self.mobileTf.rightViewMode = UITextFieldViewModeAlways;
    self.mobileTf.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mobile.png"]];
    [self.background setImage:[self gradientforview:self.background.bounds] ];

    
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)loginClicked:(id)sender
{
    LoginViewController *reg=[[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    [self.navigationController pushViewController:reg animated:YES];
}
-(IBAction)signupClicked:(id)sender
{
   
    if ([userType isEqualToString:@"DOCTOR"])
    {
        if (self.licenseTf.text==0)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please Enter a valid License Number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
    }
    
    if (self.nameTf.text.length==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please Enter User Name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];

    }
    else if (self.emailTf.text.length==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please Enter a Email Address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if (self.mobileTf.text.length==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please Enter a Mobile Number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if (self.passwordTf.text.length==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please Enter a Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    else
    {
        self.activityView.hidden=NO;
        NSString *security=[[NSUserDefaults standardUserDefaults]objectForKey:@"securityKey"];
        
    NSString *myRequestString = [[NSString alloc] initWithFormat:@"mobile=%@&name=%@&password=%@&email=%@&license_no=%@&is_doctor=%@&security_key=%@",self.mobileTf.text,self.nameTf.text,self.passwordTf.text,self.emailTf.text,self.licenseTf.text,user,security];
        
        NSLog(@"my req==%@",myRequestString);
        
        NSData *myRequestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
        
        NSMutableURLRequest *request =[[NSMutableURLRequest alloc]initWithURL:[ NSURL URLWithString:@"http://www.gigadocs.com/giga_api/signup"]];
        
        [request setHTTPMethod: @"POST"];
        
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        
        [request setHTTPBody: myRequestData];
        
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        
        
        
        NSURLSessionDataTask * dataTask1 =[defaultSession dataTaskWithRequest:request completionHandler:^(NSData* data, NSURLResponse* response, NSError *error) {                                                          if(error == nil)
            
        {
            
            self.activityView.hidden=YES;
            
            NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            self.activityView.hidden=YES;
            
            
            
            NSLog(@"reslt==%@",result);
            
            NSString *status=[result valueForKey:@"Status"];
            
            if ([status isEqualToString:@"true"])
                
            {
                
                
                
                NSDictionary *dict=[result valueForKey:@"message"];
                NSLog(@"dict==%@",dict);
                
                
                LoginViewController *reg=[[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
                [self.navigationController pushViewController:reg animated:YES];
                
                
                
            }
            
            else
                
            {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:[result valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK"otherButtonTitles: nil];
                
                [alert show];
                
            }
            
        }
            
            
            
        }];
        
        [dataTask1 resume];
    }

    

}
-(IBAction)doctorClicked:(id)sender
{
    if ([self.doctor.imageView.image isEqual:[UIImage imageNamed:@"check.png"]])
    {
        [self.doctor setImage:[UIImage imageNamed:@"scheck.png"] forState:UIControlStateNormal];
        userType=@"DOCTOR";
        user=@"YES";
        self.licenseTf.hidden=NO;

        

    }
    else
    {
        [self.doctor setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        userType=@"PATIENT";
        user=@"NO";
        self.licenseTf.hidden=YES;


        
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [textField resignFirstResponder];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.emailTf resignFirstResponder];
    [self.passwordTf resignFirstResponder];
    [self.nameTf resignFirstResponder];
    [self.licenseTf resignFirstResponder];
    [self.mobileTf resignFirstResponder];
}

-(UIImage*)gradientforview:(CGRect)bounds
{
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:0.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:52.0/255.0 green:181.0/255.0 blue:236.0/255.0 alpha:1.0].CGColor];
    
    
    gradient.startPoint=CGPointMake(0.0, 0.0);
    gradient.endPoint = CGPointMake(0.5, 1.0);
    UIGraphicsBeginImageContext(bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *aimage=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return aimage;
}

@end
