//
//  DoctorHomeController.m
//  GigaDocs
//
//  Created by RIR Technologies on 12/21/17.
//  Copyright © 2017 RIR Technologies. All rights reserved.
//

#import "DoctorHomeController.h"
#import "CalendarCell.h"
#import "MenuCellCell.h"
#import "DoctorFileController.h"
#import "CLWeeklyCalendarView.h"
#import "LoginViewController.h"
#import "GigaDocs-Bridging-Header.h"
#import "GigaDocs-Swift.h"
#import "GMDCircleLoader.h"

@interface DoctorHomeController ()<CLWeeklyCalendarViewDelegate>
{
    BOOL isMenuVisible;
 float x,y,w,z;
    NSString *date1;
    NSMutableDictionary *resultDic;
    NSMutableArray *appointmentResult;
    UIRefreshControl *refresh;
    
    NSString *hospitalID;
}

@property (nonatomic, strong) CLWeeklyCalendarView* calendarView;


@end

static CGFloat CALENDER_VIEW_HEIGHT = 120.f;

@implementation DoctorHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    resultDic=[[NSMutableDictionary alloc]init];
    appointmentResult=[[NSMutableArray alloc]init];


    
    self.activityView.hidden=YES;
    [GMDCircleLoader setOnView:self.activityView withTitle:@"" animated:YES];

    
    isMenuVisible=YES;
    self.menuTableView.frame = CGRectMake(-self.view.frame.size.width, 0, self.view.frame.size.width-100, self.view.frame.size.height);
    self.menuTableView.hidden=YES;
    self.menuView.hidden=YES;

    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.backgroundColor=[UIColor clearColor];

    
    self.menuTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    self.patientView.hidden=YES;
    [self setShadow:self.patientView];
    
    [self.background setImage:[self gradientforview:self.background.bounds] ];
    
//    self.hospitalName.rightViewMode = UITextFieldViewModeAlways;
//    self.hospitalName.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sort-down.png"]];
//
   refresh=[[UIRefreshControl alloc]init];
    refresh.tintColor=[UIColor redColor];
    refresh.attributedTitle=[[NSAttributedString alloc]initWithString:@"Pull to Refresh"];
    
    [refresh addTarget:self action:@selector(refreshClicked) forControlEvents:UIControlEventValueChanged];
    self.tableView.refreshControl=refresh;
    
    self.hospitalName.textColor=[UIColor whiteColor];
    
    [self getHospitals];



}
-(void)refreshClicked
{
    NSLog(@"calllednbvcx");
    
    NSLog(@"date===%@",date1);
    [refresh endRefreshing];
    [self getAppointments:date1];
    

}
-(void)getHospitals
{
    self.activityView.hidden=NO;
    NSString *security=[[NSUserDefaults standardUserDefaults]objectForKey:@"securityKey"];
    NSString *userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];

    
    NSString *myRequestString = [[NSString alloc] initWithFormat:@"user_id=%@&security_key=%@",userid,security];
    
    NSLog(@"my req==%@",myRequestString);
    
    NSData *myRequestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc]initWithURL:[ NSURL URLWithString:@"http://www.gigadocs.com/giga_api/get_hospital_list"]];
    
    [request setHTTPMethod: @"POST"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    [request setHTTPBody: myRequestData];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    
    
    NSURLSessionDataTask * dataTask1 =[defaultSession dataTaskWithRequest:request completionHandler:^(NSData* data, NSURLResponse* response, NSError *error) {                                                          if(error == nil)
        
    {
        
        self.activityView.hidden=YES;
        
        NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        self.activityView.hidden=YES;
        
        
        
        NSLog(@"reslt==%@",result);
        
        NSString *status=[result valueForKey:@"Status"];
        
        if ([status isEqualToString:@"true"])
            
        {
            
            
            
            resultDic=[result valueForKey:@"message"];
            NSLog(@"dict==%@",resultDic);
            
        self.hospitalName.text=[[resultDic valueForKey:@"name"] objectAtIndex:0];
          
            hospitalID=[[resultDic valueForKey:@"hospital_id"] objectAtIndex:0];
            
//            [self getAppointments];

            
            
        }
        
        else
            
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Please Try Again" message:nil delegate:self cancelButtonTitle:@"OK"otherButtonTitles: nil];
            
            [alert show];
            
        }
        
        [self.calenderParentView addSubview:self.calendarView];

    }
        
        
        
    }];
    
    [dataTask1 resume];


}
- (void)menutableDesign
{
    //    self.menuTable.hidden=YES;
    
    //    self.menuTable.frame=CGRectMake(0, 0, self.view.frame.size.width-100, window.frame.size.height);
    
    
    self.menuTableView.delegate = self;
    self.menuTableView.dataSource = self;
    self.menuTableView.opaque = YES;
    [self gradientforview:self.menuTableView.bounds];
    
    self.menuTableView.backgroundColor = [UIColor colorWithRed:81.0/255.0 green:215.0/255.0 blue:226.0/255.0 alpha:1.0];
    self.menuTableView.separatorColor=[UIColor clearColor];
    
    UIWindow * window=[[UIWindow alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    
    self.menuTableView.frame=CGRectMake(0, 0, self.menuTableView.frame.size.width, window.frame.size.height);
    
    
    self.menuTableView.delegate = self;
    self.menuTableView.dataSource = self;
    self.menuTableView.layer.borderColor=[UIColor cyanColor].CGColor;
    self.menuTableView.layer.borderWidth=1.0f;
    self.menuTableView.opaque = YES;
//    self.menuTableView.backgroundColor = [UIColor whiteColor];
    self.menuTableView.separatorColor=[UIColor whiteColor];
    self.menuTableView.tableHeaderView =
    ({
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.menuTableView.frame.size.width, 200.0f)];
        view.backgroundColor=[UIColor clearColor];
        
        
        UIButton *profile=[[UIButton alloc] initWithFrame:CGRectMake(30, 30, 100, 100)];
        
        profile.backgroundColor=[UIColor clearColor];
        
//        profile.backgroundColor=[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:225.0/255.0f alpha:0.7f];
        
        [profile setImage:[UIImage imageNamed:@"people.png"] forState:UIControlStateNormal];
        
//        [profile addTarget:self action:@selector(profileClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
    
        
     
        
      
        UILabel *nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(40, 130, self.menuTableView.frame.size.width-50, 50)];
        nameLbl.text=[NSString stringWithFormat:@"%@\n%@",        [[NSUserDefaults standardUserDefaults]objectForKey:@"name"],        [[NSUserDefaults standardUserDefaults]objectForKey:@"descrip"]];
        nameLbl.numberOfLines=0;
        nameLbl.lineBreakMode=NSLineBreakByWordWrapping;
        nameLbl.font=[UIFont boldSystemFontOfSize:15];
        nameLbl.textColor = [UIColor whiteColor];
        
      
        
        
     
        [view addSubview:profile];
        [view addSubview:nameLbl];
        
        view;
    });

    
    
    
    
}
-(CLWeeklyCalendarView *)calendarView
{
    if(!_calendarView){
        _calendarView = [[CLWeeklyCalendarView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, CALENDER_VIEW_HEIGHT)];
        _calendarView.delegate = self;
        
        
    }
    return _calendarView;
}

-(IBAction)hospitalClicked:(id)sender

{
   
    dropDownView.view.hidden=YES;

   
    NSArray *arr=[resultDic valueForKey:@"name"];
    
    NSLog(@"arry==%@",arr);
    
    
    dropDownView = [[DropDownView alloc] initWithArrayData:arr cellHeight:40 heightTableView:40*resultDic.count paddingTop:0 paddingLeft:0 paddingRight:0 refView:self.hospitalBtn animation:BLENDIN openAnimationDuration:0.5 closeAnimationDuration:0.5];
    dropDownView.view.backgroundColor=[UIColor redColor];
    dropDownView.delegate=self;
    
    [self.view addSubview:dropDownView.view];
    
//    self.hospitalName.text=[[resultDic valueForKey:@"name"] objectAtIndex:0];
    
    
    [dropDownView openAnimation];
    
}



#pragma mark - CLWeeklyCalendarViewDelegate
//-(NSDictionary *)CLCalendarBehaviorAttributes
//{
//    
//    NSCalendar *cal=[NSCalendar currentCalendar];
//    
//    NSDateComponents *comp=[cal components:NSCalendarUnitWeekday fromDate:[NSDate date]];
//    
//    NSString *day= [NSString stringWithFormat:@"%ld", (long)[comp weekday]];
//    
//    NSLog(@"day===%@",day);
//    NSLog(@"%ld",(long)[comp weekday]);
//
//    
//    
//    return @{
//             CLCalendarWeekStartDay : @1,                 //Start Day of the week, from 1-7 Mon-Sun -- default 1
////                          CLCalendarDayTitleTextColor : [UIColor yellowColor],
////                          CLCalendarSelectedDatePrintColor : [UIColor greenColor],
//             };
//}
-(void)dailyCalendarViewDidSelect:(NSDate *)date
{
    
    NSLog(@"date===%@",date);
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd-MMM-yyyy";
    
    date1=[dateFormatter stringFromDate:date];
    
    NSLog(@"%@",[dateFormatter stringFromDate:date]);
    [self getAppointments:[dateFormatter stringFromDate:date]];
    
}

-(void)getAppointments:(NSString*)date

{
    
    self.activityView.hidden=NO;
    
    NSString *security=[[NSUserDefaults standardUserDefaults]objectForKey:@"securityKey"];
    NSString *userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    
    NSString *myRequestString = [[NSString alloc] initWithFormat:@"user_id=%@&security_key=%@&date=%@&hospital_id=%@",userid,security,date,hospitalID];
    
    NSLog(@"my req==%@",myRequestString);
    
    NSData *myRequestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc]initWithURL:[ NSURL URLWithString:@"http://www.gigadocs.com/giga_api/get_calender_fordoctor"]];
    
    [request setHTTPMethod: @"POST"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    [request setHTTPBody: myRequestData];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    
    
    NSURLSessionDataTask * dataTask1 =[defaultSession dataTaskWithRequest:request completionHandler:^(NSData* data, NSURLResponse* response, NSError *error) {                                                          if(error == nil)
        
    {
        
        self.activityView.hidden=YES;
        
        NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        self.activityView.hidden=YES;
        
        
        
        NSLog(@"reslt==%@",result);
        
        NSString *status=[result valueForKey:@"Status"];
        
        if ([status isEqualToString:@"true"])
            
        {
            
            
            
            appointmentResult=[result valueForKey:@"message"];
//            NSLog(@"dict==%@",resultDic);
            
//            self.hospitalName.text=[[resultDic valueForKey:@"name"] objectAtIndex:0];
            [self.tableView reloadData];
            
            [[NSUserDefaults standardUserDefaults]setObject:[[resultDic valueForKey:@"hospital_id"]objectAtIndex:0] forKey:@"hospoita_id"];
            
            
        }
        
        else
            
        {
            
            
            appointmentResult=[[NSMutableArray alloc]init];
            [self.tableView reloadData];
//            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Please Try Again" message:nil delegate:self cancelButtonTitle:@"OK"otherButtonTitles: nil];
//            
//            [alert show];
            
        }
        
    }
        
        
        
    }];
    
    [dataTask1 resume];
    

}

-(void)dropDownCellSelected:(NSInteger)returnIndex
{
    NSLog(@"hiiii");
    
    
    NSLog(@"return index==%ld",(long)returnIndex);
    self.hospitalName.text =[[resultDic valueForKey:@"name"] objectAtIndex:returnIndex] ;
    
    hospitalID=[[resultDic valueForKey:@"hospital_id"] objectAtIndex:returnIndex];

    
    [self getAppointments:date1];
    
    dropDownView.view.hidden=YES;
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==self.menuTableView)
    {
        return 2;
    }
    else
    {
    return appointmentResult.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *identifier=@"myCell";
    if (tableView==self.tableView)
    {
        
        CalendarCell *cell=(CalendarCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"CalendarCell" owner:self options:nil];
            cell=[nib objectAtIndex:0];
        }
        
        cell.camera.tag=indexPath.row;
        [cell.camera addTarget:self action:@selector(cameraClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.viewB addTarget:self action:@selector(viewClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.viewB.tag=indexPath.row;
        cell.nameL.text=[NSString stringWithFormat:@"%@",[[appointmentResult valueForKey:@"patient_name"]objectAtIndex:indexPath.row ]];
        
        cell.timeL.text=[[appointmentResult valueForKey:@"slot_time"]objectAtIndex:indexPath.row ];
        
        
        cell.typeL.text=[[appointmentResult valueForKey:@"followup"] objectAtIndex:indexPath.row];
        
        NSString *statsu=[[appointmentResult valueForKey:@"status"] objectAtIndex:indexPath.row];
        
        if ([statsu isEqualToString:@"Closed"])
        {
            cell.backgroundColor=[UIColor colorWithRed:255.0/255.0 green:221.0/255.0 blue:204.0/255.0 alpha:1.0];
        }
        else{
            cell.backgroundColor=[UIColor whiteColor];
        }
        UISwipeGestureRecognizer *swipe=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeClicked:)];
        swipe.direction=UISwipeGestureRecognizerDirectionLeft;
        
        [cell addGestureRecognizer:swipe];
        
        
        
        
        
        UISwipeGestureRecognizer *swipe1=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(rightSwipe:)];
        swipe1.direction=UISwipeGestureRecognizerDirectionRight;
        
        [cell addGestureRecognizer:swipe1];
        
        cell.layoutMargins=UIEdgeInsetsZero;
        

        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
        
    }
    else
    {
        MenuCellCell *cell=(MenuCellCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"MenuCellCell" owner:self options:nil];
            cell=[nib objectAtIndex:0];
        }
        
        if (indexPath.row==0)
        {
            cell.titleL.text=@"Calendar";
            
        }
        else if (indexPath.row==1)
        {
            cell.titleL.text=@"Logout";
            cell.icon.image=[UIImage imageNamed:@"logout.png"];

        }
        
        cell.backgroundColor=[UIColor colorWithRed:52.0/255.0 green:181.0/255.0 blue:236.0/255.0 alpha:1.0];
        
        
        cell.layoutMargins=UIEdgeInsetsZero;

        return cell;
        
    }
}

-(void)rightSwipe:(UISwipeGestureRecognizer*)sender
{
    CGPoint location=[sender locationInView:self.tableView];
    NSIndexPath *indexpath=[self.tableView indexPathForRowAtPoint:location];

    
    SampleViewController *sample=[[SampleViewController alloc]initWithNibName:@"SampleViewController" bundle:nil];
    
    
  
    
    
    sample.dictionary=[appointmentResult objectAtIndex:indexpath.row];
    
    NSDictionary *dict=[appointmentResult objectAtIndex:indexpath.row];
    
    NSString *atr=[NSString stringWithFormat:@"Patinet name: %@ \n Phone Number: %@ \n Email :%@ \n Patient address : %@",[dict valueForKey:@"patient_name"],[dict valueForKey:@"phone_mobile"],[dict valueForKey:@"patient_emailid"],[dict valueForKey:@"patient_address"]];
    
    sample.outString=atr;
    

    [self.navigationController pushViewController:sample animated:YES];
    
    
}
-(void)swipeClicked:(UISwipeGestureRecognizer*)sender
{
//    NSString *number=[@"tel://" stringByAppendingString:[appointmentResult valueForKey:@""]obj]
    
    CGPoint location=[sender locationInView:self.tableView];
    NSIndexPath *indexpath=[self.tableView indexPathForRowAtPoint:location];
    
    if (indexpath)
    {
        NSString *num=[NSString stringWithFormat:@"tel://%@",[[appointmentResult valueForKey:@"phone_mobile"]objectAtIndex:indexpath.row]];
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:num]];
        

    }
    
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==self.menuTableView)
    {
        if (indexPath.row==0)
        {
            [self toggleComparision];

        }
        else
        {
            LoginViewController *login=[[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
            [self.navigationController pushViewController:login animated:YES];
        }
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Use Left swipe to call patient \n Right Swipe to write Prescription" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
}
-(void)cameraClicked:(UIButton*)sender
{
    DoctorFileController *reg=[[DoctorFileController alloc] initWithNibName:@"DoctorFileController" bundle:nil];
    reg.dictionary=[appointmentResult objectAtIndex:sender.tag];
    [self.navigationController pushViewController:reg animated:YES];

    
}
-(void)viewClicked:(UIButton*)sender
{
    self.patientView.hidden=NO;
    
    self.patientName.text=[NSString stringWithFormat:@"%@",[[appointmentResult valueForKey:@"patient_name"] objectAtIndex:sender.tag]];
    self.emailId.text=[NSString stringWithFormat:@"%@",[[appointmentResult valueForKey:@"patient_emailid"] objectAtIndex:sender.tag]];
    self.patientAddress.text=[NSString stringWithFormat:@"%@",[[appointmentResult valueForKey:@"patient_address"] objectAtIndex:sender.tag]];
    self.phoneNum.text=[NSString stringWithFormat:@"%@",[[appointmentResult valueForKey:@"phone_mobile"] objectAtIndex:sender.tag]];
    
    if([[[appointmentResult valueForKey:@"procedure"] objectAtIndex:sender.tag] isEqual:[NSNull null]] )
    {
        self.procedure.text = @"";
    }
    else{
    
    self.procedure.text=[NSString stringWithFormat:@"%@",[[appointmentResult valueForKey:@"procedure"] objectAtIndex:sender.tag]];

    }


    
}
-(IBAction)okClicked:(id)sender
{
    self.patientView.hidden=YES;

}
-(IBAction)menuClicked:(id)sender
{
    
    [self menutableDesign];
    [self toggleComparision];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
    isMenuVisible=NO;
    [self toggleComparision];
    
    
}
- (void)toggleComparision
{
    
    if (isMenuVisible)
    {
        
        [UIView animateWithDuration:0.4 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^ {
                             
        self.menuTableView.frame = CGRectMake(0.f, 0, self.view.frame.size.width-100, self.view.frame.size.height);
                             
                             self.menuTableView.hidden=NO;
                             self.menuView.hidden=NO;
                             
                             isMenuVisible=NO;
                         }
                         completion:^(BOOL finished){
                             
                             //                             self.menuTable.hidden=NO;
                         }];
        
    }
    else
    {
        
        [UIView animateWithDuration:0.4 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^ {
                             
            self.menuTableView.frame = CGRectMake(-self.view.frame.size.width+100, 0, self.view.frame.size.width-100, self.view.frame.size.height);
                             
                             
                         }
                         completion:^(BOOL finished)
         {

             self.menuTableView.hidden=YES;
             self.menuView.hidden=YES;
             
             isMenuVisible=YES;
         }];    }
}
-(void)setShadow:(UIView*)imag
{
    
    imag.layer.shadowOffset=CGSizeMake(0, 5);
    imag.layer.shadowRadius=5;
    imag.layer.shadowOpacity=0.6;
    imag.layer.masksToBounds=NO;
//    imag.backgroundColor=[UIColor colorWithRed:250.0/255.0 green:250.0/255.0 blue:250.0/255.0 alpha:1.0];
    imag.layer.shadowColor=[UIColor darkGrayColor].CGColor;
    
    
}
-(UIImage*)gradientforview:(CGRect)bounds
{
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:0.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:52.0/255.0 green:181.0/255.0 blue:236.0/255.0 alpha:1.0].CGColor];
    
    
    gradient.startPoint=CGPointMake(0.0, 0.0);
    gradient.endPoint = CGPointMake(0.5, 1.0);
    UIGraphicsBeginImageContext(bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *aimage=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return aimage;
}

@end
