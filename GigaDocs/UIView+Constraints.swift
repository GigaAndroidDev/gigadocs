//
//  UIView+Constraints.swift
//

import UIKit


extension UIView {
    
    // MARK: - NSLayoutConstraint Convenience Methods

    func addAutoLayoutSubview(_ subview: UIView)
    {
        addSubview(subview)
        subview.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func addAutoLayoutSubviews(_ subviews: UIView...)
    {
        for subview in subviews {
            addSubview(subview)
            subview.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    
    // MARK: - Layout
    
    func fillSuperview()
    {
        guard let superview = self.superview else { return }
        NSLayoutConstraint.activate([
            leftAnchor.constraint(equalTo: superview.leftAnchor, constant: 5),
            rightAnchor.constraint(equalTo: superview.rightAnchor, constant: 5),
            topAnchor.constraint(equalTo: superview.topAnchor, constant: 5),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: 5)
        ])
        
        
    }
    
    
}
